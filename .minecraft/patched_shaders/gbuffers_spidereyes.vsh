#version 150 core
#define MC_GL_NV_vertex_buffer_unified_memory

#define MC_GL_EXT_texture_filter_anisotropic

#define MC_GL_KHR_shader_subgroup

#define MC_GL_NV_gpu_program_fp64

#define MC_GL_ARB_timer_query

#define MC_GL_NV_register_combiners

#define MC_GL_NV_half_float

#define MC_GL_ARB_framebuffer_sRGB

#define MC_GL_ARB_texture_gather

#define MC_GL_NV_geometry_shader4

#define MC_GL_EXT_texture_edge_clamp

#define MC_GL_NV_multisample_filter_hint

#define MC_GL_VENDOR_NVIDIA

#define MC_GL_ARB_texture_rgb10_a2ui

#define MC_GL_ARB_transpose_matrix

#define MC_GL_ARB_texture_stencil8

#define MC_GL_NV_framebuffer_multisample_coverage

#define MC_GL_NV_conditional_render

#define MC_GL_NV_copy_image

#define MC_GL_ARB_vertex_buffer_object

#define MC_GL_EXT_direct_state_access

#define MC_GL_EXT_texture_object

#define MC_GL_ARB_window_pos

#define MC_GL_NV_gpu_shader5

#define MC_GL_KHR_parallel_shader_compile

#define MC_GL_ARB_point_parameters

#define MC_GL_ARB_draw_buffers_blend

#define MC_GL_EXT_semaphore

#define MC_GL_EXT_vertex_array_bgra

#define MC_GL_EXT_blend_func_separate

#define MC_GL_EXT_texture_compression_dxt1

#define MC_GL_EXT_compiled_vertex_array

#define MC_GL_EXT_blend_minmax

#define MC_GL_EXT_pixel_buffer_object

#define MC_GL_NV_shader_atomic_float64

#define MC_GL_NV_clip_space_w_scaling

#define MC_GL_NV_conservative_raster_pre_snap_triangles

#define MC_GL_ARB_shader_image_size

#define MC_GL_NV_texgen_reflection

#define MC_GL_ARB_sparse_texture2

#define MC_GL_ARB_bindless_texture

#define MC_GL_ARB_texture_buffer_object_rgb32

#define MC_GL_ARB_get_program_binary

#define MC_GL_NV_shader_subgroup_partitioned

#define MC_GL_EXTX_framebuffer_mixed_formats

#define MC_GL_EXT_shader_image_load_formatted

#define MC_GL_ARB_texture_rectangle

#define MC_GL_ARB_shading_language_100

#define MC_GL_ARB_debug_output

#define MC_GL_NV_shader_atomic_fp16_vector

#define MC_GL_NV_packed_depth_stencil

#define MC_GL_NV_conservative_raster

#define MC_GL_ARB_vertex_program

#define MC_GL_ARB_fragment_coord_conventions

#define MC_GL_ARB_compute_variable_group_size

#define MC_GL_EXT_framebuffer_blit

#define MC_GL_EXT_fog_coord

#define MC_GL_ARB_shading_language_packing

#define MC_GL_ARB_texture_filter_anisotropic

#define MC_GL_NVX_blend_equation_advanced_multi_draw_buffers

#define MC_GL_EXT_packed_float

#define MC_GL_NV_fragment_program_option

#define MC_GL_VERSION 320

#define MC_GL_ARB_texture_barrier

#define MC_VERSION 11700

#define MC_GL_ARB_buffer_storage

#define MC_GL_NVX_linked_gpu_multicast

#define MC_GL_NV_blend_square

#define MC_GL_EXT_shadow_funcs

#define MC_GL_NV_copy_depth_to_color

#define MC_GL_EXT_multiview_texture_multisample

#define MC_GL_ARB_shader_precision

#define MC_GL_ARB_shader_atomic_counter_ops

#define MC_GL_EXT_texture_env_combine

#define MC_GL_EXT_point_parameters

#define MC_OS_WINDOWS

#define MC_GL_EXT_semaphore_win32

#define MC_GL_ARB_texture_border_clamp

#define MC_GL_ARB_sparse_buffer

#define MC_GL_ARB_vertex_attrib_64bit

#define MC_GL_NV_fill_rectangle

#define MC_GL_ARB_cull_distance

#define MC_GL_NV_fragment_shader_interlock

#define MC_GL_ARB_texture_env_add

#define MC_GL_EXT_depth_bounds_test

#define MC_GL_ARB_fragment_layer_viewport

#define MC_GL_ARB_conditional_render_inverted

#define MC_GL_EXT_provoking_vertex

#define MC_GL_NV_vertex_attrib_integer_64bit

#define MC_GL_NV_shader_atomic_float

#define MC_GL_ARB_texture_query_lod

#define MC_GL_NV_stereo_view_rendering

#define MC_GL_ARB_map_buffer_alignment

#define MC_GL_NV_path_rendering

#define MC_GL_ARB_gl_spirv

#define MC_GL_NV_ES1_1_compatibility

#define MC_GL_NV_geometry_shader_passthrough

#define MC_GL_ARB_pipeline_statistics_query

#define MC_GL_EXT_bindable_uniform

#define MC_GL_ARB_shader_image_load_store

#define MC_GL_ARB_separate_shader_objects

#define MC_GL_NV_fog_distance

#define MC_GL_NV_shader_buffer_load

#define MC_GL_NV_vertex_program2

#define MC_GL_NV_texture_barrier

#define MC_GL_ARB_vertex_type_2_10_10_10_rev

#define MC_GL_NV_vertex_program3

#define MC_GL_EXT_texture_sRGB_R8

#define MC_GL_ARB_transform_feedback2

#define MC_GL_EXT_texture3D

#define MC_GL_ARB_transform_feedback3

#define MC_GL_ARB_shader_storage_buffer_object

#define MC_GL_ARB_arrays_of_arrays

#define MC_GL_NV_register_combiners2

#define MC_GL_EXT_texture_shadow_lod

#define MC_GL_EXT_packed_depth_stencil

#define MC_GL_ARB_texture_compression_rgtc

#define MC_GL_EXT_geometry_shader4

#define MC_GLSL_VERSION 150

#define MC_GL_NV_texture_rectangle_compressed

#define MC_GL_EXT_separate_specular_color

#define MC_GL_ARB_shader_bit_encoding

#define MC_GL_EXT_draw_instanced

#define MC_GL_EXT_blend_equation_separate

#define MC_GL_ARB_occlusion_query2

#define MC_GL_ARB_half_float_vertex

#define MC_GL_NV_blend_equation_advanced_coherent

#define MC_GL_NV_texture_rectangle

#define MC_GL_EXT_texture_storage

#define MC_GL_ARB_texture_rg

#define MC_GL_EXT_polygon_offset_clamp

#define MC_GL_NV_shader_atomic_counters

#define MC_GL_NV_alpha_to_coverage_dither_control

#define MC_GL_AMD_vertex_shader_layer

#define MC_GL_ARB_shader_texture_lod

#define MC_GL_NV_draw_texture

#define MC_GL_ARB_copy_image

#define MC_GL_ARB_framebuffer_object

#define MC_GL_EXT_raster_multisample

#define MC_GL_EXT_transform_feedback2

#define MC_GL_ARB_color_buffer_float

#define MC_GL_EXT_draw_range_elements

#define MC_GL_NV_compute_program5

#define MC_GL_ARB_texture_mirror_clamp_to_edge

#define MC_GL_ARB_robust_buffer_access_behavior

#define MC_GL_ARB_explicit_uniform_location

#define MC_GL_ARB_ES3_compatibility

#define MC_GL_ARB_half_float_pixel

#define MC_GL_ATI_draw_buffers

#define MC_GL_ARB_shader_ballot

#define MC_GL_NV_fence

#define MC_GL_EXT_separate_shader_objects

#define MC_GL_NV_primitive_restart

#define MC_GL_NV_feature_query

#define MC_GL_NV_transform_feedback2

#define MC_GL_EXT_texture_env_dot3

#define MC_GL_ARB_draw_buffers

#define MC_GL_EXT_import_sync_object

#define MC_GL_NV_texture_shader2

#define MC_GL_ARB_sparse_texture_clamp

#define MC_GL_ARB_compressed_texture_pixel_storage

#define MC_GL_NV_gpu_multicast

#define MC_GL_NV_blend_minmax_factor

#define MC_GL_ARB_texture_env_dot3

#define MC_GL_NV_texture_shader3

#define MC_GL_NV_path_rendering_shared_edge

#define MC_GL_KHR_no_error

#define MC_GL_NV_multisample_coverage

#define MC_GL_EXT_blend_subtract

#define MC_GL_EXT_sparse_texture2

#define MC_GL_NV_ES3_1_compatibility

#define MC_GL_EXT_secondary_color

#define MC_GL_EXT_memory_object

#define MC_GL_EXT_framebuffer_object

#define MC_GL_NV_texture_multisample

#define MC_GL_ARB_spirv_extensions

#define MC_GL_NV_sample_locations

#define MC_GL_ARB_viewport_array

#define MC_GL_ARB_transform_feedback_overflow_query

#define MC_GL_NV_occlusion_query

#define MC_GL_EXT_framebuffer_multisample

#define MC_GL_ARB_seamless_cubemap_per_texture

#define MC_GL_EXT_texture_compression_latc

#define MC_GL_NV_texture_compression_vtc

#define MC_GL_EXT_multiview_timer_query

#define MC_GL_S3_s3tc

#define MC_GL_ARB_vertex_shader

#define MC_GL_KHR_blend_equation_advanced

#define MC_GL_ARB_depth_texture

#define MC_GL_ARB_framebuffer_no_attachments

#define MC_GL_WIN_swap_hint

#define MC_GL_EXT_window_rectangles

#define MC_GL_ARB_shader_viewport_layer_array

#define MC_GL_EXT_texture_mirror_clamp

#define MC_GL_NV_parameter_buffer_object2

#define MC_GL_NV_vertex_program

#define MC_GL_ARB_sparse_texture

#define MC_GL_ARB_indirect_parameters

#define MC_GL_EXT_framebuffer_multisample_blit_scaled

#define MC_GL_ARB_invalidate_subdata

#define MC_GL_ARB_texture_swizzle

#define MC_GL_NV_viewport_swizzle

#define MC_GL_EXT_packed_pixels

#define MC_GL_AMD_multi_draw_indirect

#define MC_GL_ARB_polygon_offset_clamp

#define MC_GL_NV_conservative_raster_dilate

#define MC_GL_ARB_seamless_cube_map

#define MC_GL_ARB_geometry_shader4

#define MC_GL_EXT_texture_env_add

#define MC_GL_ARB_stencil_texturing

#define MC_GL_NV_shader_storage_buffer_object

#define MC_GL_ARB_compute_shader

#define MC_GL_ARB_shader_objects

#define MC_GL_NV_gpu_program4_1

#define MC_GL_ARB_multi_draw_indirect

#define MC_GL_ARB_parallel_shader_compile

#define MC_GL_EXT_texture_array

#define MC_GL_ARB_get_texture_sub_image

#define MC_GL_SUN_slice_accum

#define MC_GL_NV_vertex_array_range2

#define MC_GL_NV_gpu_program5_mem_extended

#define MC_GL_EXT_Cg_shader

#define MC_GL_NVX_gpu_multicast2

#define MC_GL_ARB_pixel_buffer_object

#define MC_GL_ARB_tessellation_shader

#define MC_GL_ARB_texture_cube_map_array

#define MC_GL_NV_memory_attachment

#define MC_GL_SGIX_shadow

#define MC_GL_EXT_memory_object_win32

#define MC_GL_ARB_vertex_array_bgra

#define MC_GL_NV_fragment_coverage_to_color

#define MC_GL_EXT_vertex_attrib_64bit

#define MC_GL_NVX_conditional_render

#define MC_GL_NVX_progress_fence

#define MC_GL_EXT_texture_lod_bias

#define MC_GL_EXT_rescale_normal

#define MC_GL_NV_uniform_buffer_unified_memory

#define MC_GL_NVX_multigpu_info

#define MC_GL_ARB_clear_buffer_object

#define MC_GL_EXT_win32_keyed_mutex

#define MC_GL_NV_vertex_program1_1

#define MC_GL_NV_query_resource_tag

#define MC_GL_ARB_transform_feedback_instanced

#define MC_GL_ARB_uniform_buffer_object

#define MC_GL_NV_float_buffer

#define MC_GL_KHR_blend_equation_advanced_coherent

#define MC_GL_NV_timeline_semaphore

#define MC_GL_EXT_texture_cube_map

#define MC_GL_ARB_conservative_depth

#define MC_GL_EXT_texture_shared_exponent

#define MC_GL_KHR_robust_buffer_access_behavior

#define MC_GL_IBM_rasterpos_clip

#define MC_GL_ARB_fragment_shader

#define MC_GL_ARB_ES2_compatibility

#define MC_GL_ARB_direct_state_access

#define MC_GL_ARB_draw_instanced

#define MC_GL_EXT_shader_image_load_store

#define MC_GL_ARB_gpu_shader_fp64

#define MC_GL_ARB_texture_buffer_object

#define MC_GL_NV_sample_mask_override_coverage

#define MC_GL_ARB_instanced_arrays

#define MC_GL_ARB_sync

#define MC_GL_ARB_sample_locations

#define MC_GL_NV_point_sprite

#define MC_GL_ARB_base_instance

#define MC_GL_ARB_texture_compression

#define MC_GL_ARB_gpu_shader5

#define MC_GL_NV_viewport_array2

#define MC_GL_EXT_blend_color

#define MC_GL_EXT_gpu_shader4

#define MC_GL_ARB_texture_query_levels

#define MC_GL_EXT_post_depth_coverage

#define MC_GL_ARB_texture_non_power_of_two

#define MC_GL_EXT_draw_buffers2

#define MC_GL_EXT_multi_draw_arrays

#define MC_GL_EXT_vertex_array

#define MC_GL_ARB_texture_filter_minmax

#define MC_GL_ARB_texture_buffer_range

#define MC_GL_ARB_shadow

#define MC_GL_ARB_clip_control

#define MC_GL_ARB_ES3_2_compatibility

#define MC_GL_ARB_shading_language_include

#define MC_GL_ARB_texture_compression_bptc

#define MC_GL_ARB_multisample

#define MC_GL_ARB_point_sprite

#define MC_GL_ARB_fragment_program

#define MC_GL_ARB_vertex_array_object

#define MC_GL_OVR_multiview2

#define MC_GL_EXT_texture_compression_rgtc

#define MC_WGL_EXT_swap_control

#define MC_GL_ARB_multitexture

#define MC_GL_OVR_multiview

#define MC_GL_ARB_clear_texture

#define MC_GL_NV_shader_thread_group

#define MC_GL_ARB_shader_subroutine

#define MC_GL_ARB_query_buffer_object

#define MC_GL_NV_parameter_buffer_object

#define MC_GL_ARB_ES3_1_compatibility

#define MC_GL_ARB_occlusion_query

#define MC_GL_ARB_draw_indirect

#define MC_GL_ARB_texture_view

#define MC_GL_ARB_internalformat_query

#define MC_GL_NV_draw_vulkan_image

#define MC_GL_NV_query_resource

#define MC_GL_EXT_texture_filter_minmax

#define MC_GL_NV_framebuffer_mixed_samples

#define MC_GL_KHR_robustness

#define MC_GL_EXT_texture_sRGB_decode

#define MC_GL_NVX_gpu_memory_info

#define MC_GL_ARB_shading_language_420pack

#define MC_GL_ATI_texture_mirror_once

#define MC_GL_EXT_texture_lod

#define MC_GL_EXT_framebuffer_sRGB

#define MC_GL_NV_memory_object_sparse

#define MC_GL_NV_texture_env_combine4

#define MC_GL_ARB_texture_multisample

#define MC_GL_ARB_sample_shading

#define MC_GL_ARB_draw_elements_base_vertex

#define MC_GL_EXT_gpu_program_parameters

#define MC_GL_KHR_context_flush_control

#define MC_GL_AMD_seamless_cubemap_per_texture

#define MC_GL_ARB_depth_buffer_float

#define MC_GL_NV_internalformat_sample_query

#define MC_GL_ARB_texture_storage

#define MC_GL_ARB_provoking_vertex

#define MC_GL_ARB_vertex_attrib_binding

#define MC_GL_ARB_texture_env_crossbar

#define MC_GL_ARB_fragment_program_shadow

#define MC_GL_NV_shader_atomic_int64

#define MC_GL_NV_explicit_multisample

#define MC_GL_NV_light_max_exponent

#define MC_GL_ARB_texture_float

#define MC_GL_ARB_texture_storage_multisample

#define MC_GL_ARB_blend_func_extended

#define MC_GL_KTX_buffer_region

#define MC_GL_ARB_fragment_shader_interlock

#define MC_GL_ARB_derivative_control

#define MC_GL_NV_blend_equation_advanced

#define MC_GL_ARB_depth_clamp

#define MC_GL_ARB_shader_atomic_counters

#define MC_GL_EXT_bgra

#define MC_GL_ARB_texture_cube_map

#define MC_GL_ARB_shader_texture_image_samples

#define MC_GL_NV_vertex_array_range

#define MC_GL_ARB_shader_draw_parameters

#define MC_GL_ARB_imaging

#define MC_GL_ARB_texture_mirrored_repeat

#define MC_GL_NV_fragment_program

#define MC_GL_NV_depth_buffer_float

#define MC_GL_EXT_texture_buffer_object

#define MC_GL_SGIX_depth_texture

#define MC_GL_ARB_vertex_type_10f_11f_11f_rev

#define MC_GL_EXT_texture_integer

#define MC_GL_EXT_texture_compression_s3tc

#define MC_GL_KHR_debug

#define MC_GL_ARB_robustness

#define MC_GL_EXT_shader_integer_mix

#define MC_GL_NV_fragment_program2

#define MC_GL_NV_pixel_data_range

#define MC_GL_EXT_timer_query

#define MC_GL_ARB_shader_group_vote

#define MC_GL_ARB_internalformat_query2

#define MC_GL_IBM_texture_mirrored_repeat

#define MC_GL_NV_bindless_multi_draw_indirect_count

#define MC_GL_NV_vertex_program2_option

#define MC_GL_ARB_sampler_objects

#define MC_GL_EXT_stencil_wrap

#define MC_GL_ARB_map_buffer_range

#define MC_GL_EXT_texture_swizzle

#define MC_GL_ARB_program_interface_query

#define MC_GL_ARB_enhanced_layouts

#define MC_GL_ARB_texture_env_combine

#define MC_GL_ARB_post_depth_coverage

#define MC_GL_ATI_texture_float

#define MC_GL_NV_command_list

#define MC_GL_SGIS_generate_mipmap

#define MC_GL_ARB_copy_buffer

#define MC_GL_NV_shader_thread_shuffle

#define MC_GL_NV_depth_clamp

#define MC_GL_AMD_vertex_shader_viewport_index

#define MC_GL_NVX_nvenc_interop

#define MC_GL_ARB_multi_bind

#define MC_GL_SGIS_texture_lod

#define MC_GL_ARB_shader_clock

#define MC_GL_EXT_abgr

#define MC_GL_NV_bindless_texture

#define MC_GL_NV_transform_feedback

#define MC_GL_EXT_stencil_two_side

#define MC_GL_NV_bindless_multi_draw_indirect

#define MC_GL_NV_texture_shader

#define MC_GL_RENDERER_GEFORCE

#define MC_GL_ARB_explicit_attrib_location

#define MC_GL_ARB_gpu_shader_int64

#define MC_GL_NV_gpu_program4

#define MC_GL_EXT_texture_sRGB

#define MC_GL_NV_gpu_program5

#define varying out
#define attribute in
#define gl_Vertex vec4(Position, 1.0)
#define gl_ModelViewProjectionMatrix (gl_ProjectionMatrix * gl_ModelViewMatrix)
#define gl_ModelViewMatrix (iris_ModelViewMat * _iris_internal_translate(iris_ChunkOffset))
#define gl_NormalMatrix mat3(transpose(inverse(gl_ModelViewMatrix)))
#define gl_Normal Normal
#define gl_Color (Color * iris_ColorModulator)
#define gl_MultiTexCoord7  vec4(0.0, 0.0, 0.0, 1.0)
#define gl_MultiTexCoord6  vec4(0.0, 0.0, 0.0, 1.0)
#define gl_MultiTexCoord5  vec4(0.0, 0.0, 0.0, 1.0)
#define gl_MultiTexCoord4  vec4(0.0, 0.0, 0.0, 1.0)
#define gl_MultiTexCoord3  vec4(0.0, 0.0, 0.0, 1.0)
#define gl_MultiTexCoord2  vec4(0.0, 0.0, 0.0, 1.0)
#define gl_MultiTexCoord1 vec4(UV2, 0.0, 1.0)
#define gl_MultiTexCoord0 vec4(UV0, 0.0, 1.0)
#define gl_ProjectionMatrix iris_ProjMat
#define gl_FrontColor iris_FrontColor
#define gl_FogFragCoord iris_FogFragCoord
uniform mat4 iris_LightmapTextureMatrix;
uniform mat4 iris_TextureMat;
uniform float iris_FogDensity;
uniform float iris_FogStart;
uniform float iris_FogEnd;
uniform vec4 iris_FogColor;

struct iris_FogParameters {
    vec4 color;
    float density;
    float start;
    float end;
    float scale;
};

iris_FogParameters iris_Fog = iris_FogParameters(iris_FogColor, iris_FogDensity, iris_FogStart, iris_FogEnd, 1.0 / (iris_FogEnd - iris_FogStart));

#define gl_Fog iris_Fog
out float iris_FogFragCoord;
vec4 iris_FrontColor;
uniform mat4 iris_ProjMat;
in vec2 UV0;
in ivec2 UV2;
uniform vec4 iris_ColorModulator;
in vec4 Color;
in vec3 Normal;
uniform mat4 iris_ModelViewMat;
uniform vec3 iris_ChunkOffset;
mat4 _iris_internal_translate(vec3 offset) {
    // NB: Column-major order
    return mat4(1.0, 0.0, 0.0, 0.0,
                0.0, 1.0, 0.0, 0.0,
                0.0, 0.0, 1.0, 0.0,
                offset.x, offset.y, offset.z, 1.0);
}
in vec3 Position;
vec4 ftransform() { return gl_ModelViewProjectionMatrix * gl_Vertex; }
vec4 texture2D(sampler2D sampler, vec2 coord) { return texture(sampler, coord); }
vec4 texture2DLod(sampler2D sampler, vec2 coord, float lod) { return textureLod(sampler, coord, lod); }
vec4 shadow2D(sampler2DShadow sampler, vec3 coord) { return vec4(texture(sampler, coord)); }
vec4 shadow2DLod(sampler2DShadow sampler, vec3 coord, float lod) { return vec4(textureLod(sampler, coord, lod)); }

#define composite2
#define gbuffers_texturedblock
/*
Thank you for downloading Sildur's vibrant shaders, make sure you got it from the official source found here:
https://sildurs-shaders.github.io/
*/
#ifdef gbuffers_shadows
    #define Shadows								//Toggle all shadows
    #define SHADOW_MAP_BIAS 0.80
    #define grass_shadows                       //Also disables tallgrass and flowers shadows
    #ifdef Shadows
    const float shadowDistance = 70.0;			//Render distance of shadows. 60=lite, 80=med, 80=high, 120=extreme [60.0 70.0 80.0 90.0 100.0 110.0 120.0 130.0 140.0 150.0 160.0 170.0 180.0 190.0 200.0 210.0 220.0 230.0 240.0 250.0 260.0 270.0 280.0 290.0 300.0 310.0 320.0 330.0 340.0 350.0 360.0 370.0 380.0 390.0 400.0]
    const int shadowMapResolution = 512;		//Shadows resolution. [256 512 1024 2048 3072 4096 6144 8192 16384] 512=lite, 1024=med, 2048=high, 3072=extreme 
    const float k = 1.8;
    #define Nearshadowplane 0.05	            //[0.04 0.045 0.05 0.055 0.06 0.065 0.07 0.075 0.08 0.085 0.09 0.095 0.1] close quality, lower=higher quality, 0.09 is required for 2.0 farshadowmap to prevent glitches
    #define Farshadowplane 1.4                  //[0.6 0.8 1.0 1.2 1.4 1.6 1.8 2.0] far quality, streches the shadowmap, lower values closer, higher further away: 0.8=extreme, 1.0=high, 1.2=medium, 1.4=lite
    float a = exp(Nearshadowplane);
    float b = (exp(Farshadowplane)-a)*shadowDistance/128.0;
    float calcDistortion(vec2 worldpos){
        return 1.0/(log(length(worldpos)*b+a)*k);
    }
    #endif
#endif

#ifdef composite0
    #define ColoredShadows						//Toggle colored shadows
    //#define Penumbra                            //Toggle penumbra soft shadows
    //#define raytracedShadows                  //Improves closeup and faraway shadows. Also allows shadows to be cast outside of the shadowmap, outside of your shadows render distance. Requires shadows to be enabled. Has some issues since it's raytraced in screenspace.
    const int VPS_samples = 8;                  //Used for penumbra shadows [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30]
    #define shadow_samples 10                   //Used for shadows in general [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30]
    const bool 	shadowHardwareFiltering0 = true;
    const bool 	shadowHardwareFiltering1 = true;
    const float	sunPathRotation	= -40.0;		//[-10.0 -20.0 -30.0 -40.0 -50.0 -60.0 -70.0 -80.0 0.0 10.0 20.0 30.0 40.0 50.0 60.0 70.0 80.0]
    
    //#define SSDO				                //Ambient Occlusion, makes lighting more realistic. High performance impact.
        #define ao_strength 3.0                 //[1.0 2.0 3.0 4.0 5.0 6.0 7.0 8.0 9.0 10.0]

    #define Godrays
        const int grays_sample = 17;            //17=lite, 17=med, 20=high, 23=extreme
    //#define Volumetric_Lighting               //Disable godrays before enabling volumetric lighting.

    //#define Lens_Flares

    //#define Celshading                        //Cel shades everything, making it look somewhat like Borderlands. Zero performance impact.
        #define Celborder 1.0                   //[1.0 1.2 1.4 1.6 1.8 2.0 2.2 2.4 2.6 2.8 3.0]
	    #define Celradius 1.0                   //[1.0 0.9 0.8 0.7 0.6 0.5 0.4 0.3 0.2 0.1 0.075 0.05]

    //#define Whiteworld                        //Makes the ground white, screenshot -> https://i.imgur.com/xziUB8O.png

    #define Moonlight 0.003                     //[0.0 0.0015 0.003 0.006 0.009]

    //#define defskybox

    #define emissive_R 1.5                      //[0.0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0]
    #define emissive_G 0.42                     //[0.0 0.1 0.2 0.3 0.42 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0]
    #define emissive_B 0.045                    //[0.0 0.045 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0]
    #define minlight 0.002                      //Tweak the amount of minimal light inside caves etc. [0.001 0.002 0.003 0.004 0.005 0.006 0.007 0.008 0.009 0.010 0.011 0.012 0.013 0.014 0.015 0.016 0.017 0.018 0.019 0.020 0.021 0.022 0.023 0.024 0.025 0.026 0.027 0.028 0.029 0.030]
    
    #ifdef Penumbra
        //required for optifine to parse it since it doesn't parse #if defined penumbra, nvidia, windows.
    #endif

    //Use the same color as water for water shading, diffuse
    #define waterCR 0.0	        //[0.0 0.1 0.125 0.15 0.175 0.2 0.225 0.25 0.275 0.3 0.325 0.35 0.375 0.4 0.6 0.8 1.0 1.2 1.4 1.6 1.8 2.0 2.2 2.4 2.6 2.8 3.0]
    #define waterCG 0.175	    //[0.0 0.1 0.125 0.15 0.175 0.2 0.225 0.25 0.275 0.3 0.325 0.35 0.375 0.4 0.6 0.65 0.8 1.0 1.2 1.4 1.6 1.8 2.0 2.2 2.4 2.6 2.8 3.0]
    #define waterCB 0.2	        //[0.0 0.1 0.125 0.15 0.175 0.2 0.225 0.25 0.275 0.3 0.325 0.35 0.375 0.4 0.6 0.8 1.0 1.2 1.25 1.4 1.6 1.8 2.0 2.2 2.4 2.6 2.8 3.0]    
#endif

#ifdef composite1
	const int noiseTextureResolution = 128;     //must be in composite1 for 2d clouds
    
	#define Godrays
		#define Godrays_Density 1.15            //[0.575 1.15 2.3 4.6 9.2]
	//#define Lens_Flares

	//#define Volumetric_Lighting               //Disable godrays before enabling volumetric lighting.
	
	#define Fog                                 //Toggle fog
		#define wFogDensity	100.0               //adjust fog density [5.0 10.0 15.0 20.0 25.0 30.0 35.0 40.0 45.0 50.0 55.0 60.0 65.0 70.0 75.0 80.0 90.0 100.0 110.0 120.0 130.0 140.0 150.0 160.0 170.0 180.0 190.0 200.0 210.0 220.0 230.0 240.0 250.0 260.0 270.0 280.0 290.0 300.0]		
        //#define morningFog                    //Toggle dynamic fog during sunrise.
    #define Underwater_Fog                      //Toggle underwater fog. 
		#define uFogDensity 25.0                //adjust underwater fog density [5.0 10.0 15.0 20.0 25.0 30.0 35.0 40.0 45.0 50.0 55.0 60.0 65.0 70.0 75.0 80.0 90.0 100.0 110.0 120.0 130.0 140.0 150.0 160.0 170.0 180.0 190.0 200.0]		
		#define uwatertint                      //Tints the underwater ground with blue color.

	#define Clouds 3                            //[0 1 2 3 4] Toggle clouds. 0=Off, 1=Default MC, 2=2D, 3=VL, 4=2D+VL, also adjust in gbuffers_cloud
		#define cloudsIT 6                      //[6 8 10 12 14 16 18 20 24 32 48] Volumetric clouds quality.
		#define cloudreflIT 2                   //[2 4 6 8 10 12 14 16] Reflected volumetric clouds quality.
		//#define Cloud_reflection              //Toggle clouds reflection in water	
        #define cloud_height 256.0		        //[64.0 80.0 96.0 112.0 128.0 144.0 160.0 176.0 192.0 208.0 224.0 240.0 256.0 272.0 288.0 304.0 320.0 336.0 352.0 368.0 384.0 400.0]

	//#define waterRefl                           //Toggle water reflections
	//#define iceRefl                             //Toggle stained glass and ice reflections
	
    //#define Refraction                          //Toggle water refractions.
    //#define Caustics                            //Toggle water caustics.
    #define causticsStrength 0.8                //[0.1 0.15 0.2 0.25 0.30 0.35 0.4 0.45 0.5 0.55 0.6 0.65 0.7 0.75 0.8 0.85 0.9 0.95 1.0 1.05 1.10 1.15 1.20 1.25 1.3 1.35 1.4 1.45 1.5 1.55 1.6 1.65 1.75 1.8 1.85 1.9 1.95 2.0]
    #define waveSize 1.0                        //[0.25 0.5 0.75 1.0 1.25 1.5 1.75 2.0 2.25 2.5 2.75 3.0] Adjust water wave size, includes refraction and caustic size.

    //#define metallicRefl                        //Toggle reflections for metallic blocks defined in block.properties
    //#define polishedRefl                        //Toggle reflections for polished blocks defined in block.properties   
    #define metalStrength 1.0                   //[0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0] Metallic and polished reflection strength
    #define metallicSky 0.1                     //[0.0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0] Sky reflection strength

	//#define RainReflections                   //Toggle rain reflections, wetness
	#define BiomeCheck                          //Toggle biome check for rain reflections. Only enable if no reflections are present. Updating optifine might resolve missing reflections with this on.
	#define rainNoise 1.0                       //[1.0 2.0 3.0 4.0 5.0 6.0 7.0 8.0 9.0 10.0] Higher values means less noise

	#define customStars                         //Toggle custom stars.	

    //#define defskybox                         //Toggle support for the default skybox, including custom skies from resourcepacks. If no resourcepack is present this will only enable the default sun and moon texture. This option also auto disables the custom shader sun and moon.
    #define skyboxblendfactor 0.75              //[0.0 0.05 0.1 0.15 0.2 0.25 0.3 0.35 0.4 0.45 0.5 0.55 0.6 0.65 0.7 0.75 0.8 0.85 0.9 0.95 1.0] Adjust the blend factor of custom sky and default skybox

	#define emissive_R 1.5                      //[0.0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0]
	#define emissive_G 0.42                     //[0.0 0.1 0.2 0.3 0.42 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0]
	#define emissive_B 0.045                    //[0.0 0.045 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0]

    #ifdef Cloud_reflection
        //required for optifine to parse it since it doesn't parse #if defined
    #endif  
    #ifdef waterRefl
        //required for optifine to parse it since it doesn't parse #if defined
    #endif    
#endif

#ifdef composite2
    //#define TAA							        //Toggle temporal anti-aliasing (TAA)
    #define TAA_quality	2				        //[1 2] Fast is more blurry during movement compared to fancy.

    //#define Adaptive_sharpening			    //Toggle adaptive sharpening. Recommended to use with TAA. Disabling TAA also disables adaptive sharpening.
    #define AS_sharpening 0.5 			        //[0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0] Adjust sharpening strength.
#endif

#ifdef composite3
    //#define Bloom
#endif    

#ifdef composite4
    //#define Bloom				                //Also disables sun glare
    #define bloom_strength 0.75	                //Adjust bloom strength [0.5 0.75 1.0 2.0]
#endif

#ifdef final
    //#define Bloom

    #define Rain_Drops			                //Enables rain drops on screen during raining. Requires sun effects to be enabled. Low performance impact.

    //#define Refraction

    //#define Depth_of_Field	                //Simulates eye focusing on objects. Low performance impact
	    //#define Distance_Blur	                //Requires Depth of Field to be enabled. Replaces eye focusing effect with distance being blurred instead.
        #define smoothDof                       //Toggle smooth transition between clear and blurry.

    //#define Motionblur		                //Blurres your view/camera during movemenent. Low performance impact. Doesn't work with Depth of Field.

    #define Cloudsblur			                //Blurres the sky abit, making volumetric clouds less noisy. Doesn't work with MB and DoF
	
    //Defined values for Optifine
    #define DoF_Strength 90		                //[10 20 30 40 50 60 70 80 90 100 110 120 130 140 150 160 170 180 190 200 210 220 230 240 250 260 270 280 290 300]
    #define Dof_Distance_View 256               //[128 256 384 512]
    #define MB_strength 0.014	                //[0.008 0.014 0.020]
    #define Contrast 2.2                        //Lower values increase contrast while higher values reduce conrast. If you adjust contrast make sure adjust brightness aswell. [1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0 2.1 2.2 2.3 2.4 2.5 2.6 2.7 2.8 2.9 3.0 3.1 3.2 3.3 3.4 3.5 3.6 3.7 3.8 3.9 4.0 4.1 4.2 4.3 4.4 4.5 4.6 4.7 4.8 4.9 5.0] 
	#define Brightness 1.0                      //Adjust brightness. [0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0 2.1 2.2 2.3 2.4 2.5 2.6 2.7 2.8 2.9 3.0 3.1 3.2 3.3 3.4 3.5 3.6 3.7 3.8 3.9 4.0 4.1 4.2 4.3 4.4 4.5 4.6 4.7 4.8 4.9 5.0]

    #define Showbuffer 0	                    //[0 1 2 25 3 35 4 5 6 7 8]
#endif
 
#ifdef gbuffers_skytextured
    //#define defskybox
#endif

#ifdef gbuffers_terrain
    #define nMap 0				//[0 1 2]0=Off 1=Bumpmapping, 2=Parallax
    #define POM_RES 32			//Texture / Resourcepack resolution. [32 64 128 256 512 1024 2048]
    #define POM_DIST 16.0		//[8.0 16.0 24.0 32.0 40.0 48.0 56.0 64.0 72.0 80.0 88.0 96.0 104.0 112.0 120.0 128.0]
    #define POM_DEPTH 0.30		//[0.05 0.10 0.15 0.20 0.25 0.30 0.35 0.40 0.45 0.50 0.55 0.60 0.65 0.70 0.75 0.80 0.85 0.90 0.95 1.0]

    //#define metallicRefl        //Toggle reflections for blocks defined in block.properties
    //#define polishedRefl        //Toggle reflections for polished blocks defined in block.properties

    #define Waving_Leaves
    #define Waving_Vines
    #define Waving_Grass		//Does not include tallgrass due some issues with it.
    #define Waving_Tallgrass
    #define Waving_Fire
    #define Waving_Lava
    #define Waving_Lilypads
    #define Waving_Lanterns   
    #define Waving_Entities		//Includes: Saplings, small flowers, wheat, carrots, potatoes and beetroot.
    #define animationSpeed 1.0  //[0.25 0.5 0.75 1.0 1.25 1.5 1.75 2.0]
#endif

#ifdef gbuffers_texturedblock   //textured and block are mostly the same so lets use the same defines
    #define MobsFlashRed
    #define emissive_R 1.5		//[0.0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0]
    #define emissive_G 0.42		//[0.0 0.1 0.2 0.3 0.42 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0]
    #define emissive_B 0.045	//[0.0 0.045 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0]
    #define minlight 0.002      //Tweak the amount of minimal light inside caves etc. [0.001 0.002 0.003 0.004 0.005 0.006 0.007 0.008 0.009 0.010 0.011 0.012 0.013 0.014 0.015 0.016 0.017 0.018 0.019 0.020 0.021 0.022 0.023 0.024 0.025 0.026 0.027 0.028 0.029 0.030]
#endif

#ifdef gbuffers_water
    #define Waving_Water
    #define waves_amplitude 0.65    //[0.55 0.65 0.75 0.85 0.95 1.05 1.15 1.25 1.35 1.45 1.55 1.65 1.75 1.85 1.95 2.05]
    #define waveSize 1.0            //[0.25 0.5 0.75 1.0 1.25 1.5 1.75 2.0 2.25 2.5 2.75 3.0] Adjust water wave size, includes refraction and caustic size.

    //#define watertex
    #define wtexblend 0.12      //[0.02 0.03 0.04 0.05 0.06 0.07 0.08 0.09 0.10 0.11 0.12 0.13 0.14 0.15 0.16 0.17 0.18 0.19 0.20 0.21 0.22 0.23 0.24 0.25 0.26 0.27 0.28 0.29 0.30 0.31 0.32 0.33 0.34 0.35 0.36 0.37 0.38 0.39 0.40 0.41 0.42 0.43 0.44 0.45 0.46 0.47 0.48 0.49 0.50]
    //#define WaterParallax
    #define waterheight 1.0     //[1.0 1.5 2.0] height for parallax mapping

    #define waterCR 0.0	        //[0.0 0.1 0.125 0.15 0.175 0.2 0.225 0.25 0.275 0.3 0.325 0.35 0.375 0.4 0.6 0.8 1.0 1.2 1.4 1.6 1.8 2.0 2.2 2.4 2.6 2.8 3.0]
    #define waterCG 0.175	    //[0.0 0.1 0.125 0.15 0.175 0.2 0.225 0.25 0.275 0.3 0.325 0.35 0.375 0.4 0.6 0.65 0.8 1.0 1.2 1.4 1.6 1.8 2.0 2.2 2.4 2.6 2.8 3.0]
    #define waterCB 0.2	        //[0.0 0.1 0.125 0.15 0.175 0.2 0.225 0.25 0.275 0.3 0.325 0.35 0.375 0.4 0.6 0.8 1.0 1.2 1.25 1.4 1.6 1.8 2.0 2.2 2.4 2.6 2.8 3.0]
    #define waterA 6.0	        //[1.0 1.5 2.0 2.5 3.0 3.5 4.0 4.5 5.0 5.5 6.0 7.0 8.0 9.0 10.0]

    #define emissive_R 1.5		//[0.0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0]
    #define emissive_G 0.42		//[0.0 0.1 0.2 0.3 0.42 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0]
    #define emissive_B 0.045	//[0.0 0.045 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0]
#endif

#ifdef gbuffers_weather
    //#define WeatherAngle		//Toggle alternative weather angle
#endif

#ifdef lightingColors
	#define light_brightness 1.0 //[0.01 0.02 0.03 0.04 0.05 0.06 0.07 0.08 0.09 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0]
    #define r_multiplier 0.0	//[0.0 0.25 0.5 0.75 1.0 1.25 1.5 1.75 2.0 2.25 2.5 2.75 3.0 3.25 3.5 3.75 4.0 4.25 4.5 4.75 5.0 5.25 5.5 5.75 6.0 6.25 6.5 6.75 7.0 7.25 7.5 7.75 8.0 8.25 8.5 8.75 9.0 9.25 9.5 9.75 10.0]
	#define g_multiplier 0.0	//[0.0 0.25 0.5 0.75 1.0 1.25 1.5 1.75 2.0 2.25 2.5 2.75 3.0 3.25 3.5 3.75 4.0 4.25 4.5 4.75 5.0 5.25 5.5 5.75 6.0 6.25 6.5 6.75 7.0 7.25 7.5 7.75 8.0 8.25 8.5 8.75 9.0 9.25 9.5 9.75 10.0]
	#define b_multiplier 0.0	//[0.0 0.25 0.5 0.75 1.0 1.25 1.5 1.75 2.0 2.25 2.5 2.75 3.0 3.25 3.5 3.75 4.0 4.25 4.5 4.75 5.0 5.25 5.5 5.75 6.0 6.25 6.5 6.75 7.0 7.25 7.5 7.75 8.0 8.25 8.5 8.75 9.0 9.25 9.5 9.75 10.0]
#endif

varying vec4 color;
varying vec2 texcoord;
varying vec3 ambientNdotL;

uniform vec3 sunPosition;
uniform vec3 upPosition;

uniform int worldTime;
uniform float rainStrength;
uniform float nightVision;

uniform mat4 gbufferModelView;
uniform mat4 gbufferModelViewInverse;

const vec3 ToD[7] = vec3[7](  vec3(0.58597,0.15,0.02),
								vec3(0.58597,0.35,0.09),
								vec3(0.58597,0.5,0.26),
								vec3(0.58597,0.5,0.35),
								vec3(0.58597,0.5,0.36),
								vec3(0.58597,0.5,0.37),
								vec3(0.58597,0.5,0.38));
#ifdef TAA
uniform float viewWidth;
uniform float viewHeight;
vec2 texelSize = vec2(1.0/viewWidth,1.0/viewHeight);
uniform int framemod8;
const vec2[8] offsets = vec2[8](vec2(1./8.,-3./8.),
								vec2(-1.,3.)/8.,
								vec2(5.0,1.)/8.,
								vec2(-3,-5.)/8.,
								vec2(-5.,5.)/8.,
								vec2(-7.,-1.)/8.,
								vec2(3,7.)/8.,
								vec2(7.,-7.)/8.);
#endif							

void main() {

	color = gl_Color;
	vec4 position = gbufferModelViewInverse * gl_ModelViewMatrix * gl_Vertex;
	gl_Position = gl_ProjectionMatrix * gbufferModelView * position;

#ifdef TAA
	gl_Position.xy += offsets[framemod8] * gl_Position.w*texelSize;
#endif	
	vec3 normal = normalize(gl_NormalMatrix * gl_Normal);	
	texcoord = (iris_TextureMat * gl_MultiTexCoord0).xy;
	vec2 lmcoord = (iris_LightmapTextureMatrix * gl_MultiTexCoord1).xy;
	/*--------------------------------*/
	
	//Emissive blocks lighting
	float torch_lightmap = 16.0-min(15.,(lmcoord.s-0.5/16.)*16.*16./15);
	float fallof1 = clamp(1.0 - pow(torch_lightmap/16.0,4.0),0.0,1.0);
	torch_lightmap = fallof1*fallof1/(torch_lightmap*torch_lightmap+1.0);
	//vec3 emissiveLightC = vec3(emissive_R,emissive_G,emissive_B)*torch_lightmap;
	vec3 emissiveLightC = vec3(0.5, 0.0, 1.0); //purple eyes
	float finalminlight = (nightVision > 0.9)? 0.025 : (minlight+0.006)*10.0; //multiply by 10 to improve eye rendering

	ambientNdotL.rgb = emissiveLightC + finalminlight;

}

