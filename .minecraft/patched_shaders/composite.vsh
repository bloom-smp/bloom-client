#version 150 core
#define varying out
#define attribute in
#define gl_Vertex vec4(Position, 1.0)
#define gl_ModelViewProjectionMatrix (gl_ProjectionMatrix * gl_ModelViewMatrix)
#define gl_ModelViewMatrix mat4(1.0)
#define gl_NormalMatrix mat3(1.0)
#define gl_Normal vec3(0.0, 0.0, 1.0)
#define gl_Color vec4(1.0, 1.0, 1.0, 1.0)
#define gl_MultiTexCoord7 vec4(0.0, 0.0, 0.0, 1.0)
#define gl_MultiTexCoord6 vec4(0.0, 0.0, 0.0, 1.0)
#define gl_MultiTexCoord5 vec4(0.0, 0.0, 0.0, 1.0)
#define gl_MultiTexCoord4 vec4(0.0, 0.0, 0.0, 1.0)
#define gl_MultiTexCoord3 vec4(0.0, 0.0, 0.0, 1.0)
#define gl_MultiTexCoord2 vec4(0.0, 0.0, 0.0, 1.0)
#define gl_MultiTexCoord1 vec4(0.0, 0.0, 0.0, 1.0)
#define gl_MultiTexCoord0 vec4(UV0, 0.0, 1.0)
#define gl_ProjectionMatrix mat4(1.0)
#define gl_FogFragCoord iris_FogFragCoord
uniform float iris_FogDensity;
uniform float iris_FogStart;
uniform float iris_FogEnd;
uniform vec4 iris_FogColor;

struct iris_FogParameters {
    vec4 color;
    float density;
    float start;
    float end;
    float scale;
};

iris_FogParameters iris_Fog = iris_FogParameters(iris_FogColor, iris_FogDensity, iris_FogStart, iris_FogEnd, 1.0 / (iris_FogEnd - iris_FogStart));

#define gl_Fog iris_Fog
out float iris_FogFragCoord;
in vec2 UV0;
in vec3 Position;
vec4 ftransform() { return gl_ModelViewProjectionMatrix * gl_Vertex; }
vec4 texture2D(sampler2D sampler, vec2 coord) { return texture(sampler, coord); }
vec4 texture2DLod(sampler2D sampler, vec2 coord, float lod) { return textureLod(sampler, coord, lod); }
vec4 shadow2D(sampler2DShadow sampler, vec3 coord) { return vec4(texture(sampler, coord)); }
vec4 shadow2DLod(sampler2DShadow sampler, vec3 coord, float lod) { return vec4(textureLod(sampler, coord, lod)); }

#define Lens_Flares_Strength 4.0 //Adjust the lens flare strength [1.0 4.0 16.0 32.0 64.0]

#define lightingColors
/*
Thank you for downloading Sildur's vibrant shaders, make sure you got it from the official source found here:
https://sildurs-shaders.github.io/
*/
#ifdef gbuffers_shadows
    #define Shadows								//Toggle all shadows
    #define SHADOW_MAP_BIAS 0.80
    #define grass_shadows                       //Also disables tallgrass and flowers shadows
    #ifdef Shadows
    const float shadowDistance = 70.0;			//Render distance of shadows. 60=lite, 80=med, 80=high, 120=extreme [60.0 70.0 80.0 90.0 100.0 110.0 120.0 130.0 140.0 150.0 160.0 170.0 180.0 190.0 200.0 210.0 220.0 230.0 240.0 250.0 260.0 270.0 280.0 290.0 300.0 310.0 320.0 330.0 340.0 350.0 360.0 370.0 380.0 390.0 400.0]
    const int shadowMapResolution = 512;		//Shadows resolution. [256 512 1024 2048 3072 4096 6144 8192 16384] 512=lite, 1024=med, 2048=high, 3072=extreme 
    const float k = 1.8;
    #define Nearshadowplane 0.05	            //[0.04 0.045 0.05 0.055 0.06 0.065 0.07 0.075 0.08 0.085 0.09 0.095 0.1] close quality, lower=higher quality, 0.09 is required for 2.0 farshadowmap to prevent glitches
    #define Farshadowplane 1.4                  //[0.6 0.8 1.0 1.2 1.4 1.6 1.8 2.0] far quality, streches the shadowmap, lower values closer, higher further away: 0.8=extreme, 1.0=high, 1.2=medium, 1.4=lite
    float a = exp(Nearshadowplane);
    float b = (exp(Farshadowplane)-a)*shadowDistance/128.0;
    float calcDistortion(vec2 worldpos){
        return 1.0/(log(length(worldpos)*b+a)*k);
    }
    #endif
#endif

#ifdef composite0
    #define ColoredShadows						//Toggle colored shadows
    //#define Penumbra                            //Toggle penumbra soft shadows
    //#define raytracedShadows                  //Improves closeup and faraway shadows. Also allows shadows to be cast outside of the shadowmap, outside of your shadows render distance. Requires shadows to be enabled. Has some issues since it's raytraced in screenspace.
    const int VPS_samples = 8;                  //Used for penumbra shadows [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30]
    #define shadow_samples 10                   //Used for shadows in general [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30]
    const bool 	shadowHardwareFiltering0 = true;
    const bool 	shadowHardwareFiltering1 = true;
    const float	sunPathRotation	= -40.0;		//[-10.0 -20.0 -30.0 -40.0 -50.0 -60.0 -70.0 -80.0 0.0 10.0 20.0 30.0 40.0 50.0 60.0 70.0 80.0]
    
    //#define SSDO				                //Ambient Occlusion, makes lighting more realistic. High performance impact.
        #define ao_strength 3.0                 //[1.0 2.0 3.0 4.0 5.0 6.0 7.0 8.0 9.0 10.0]

    #define Godrays
        const int grays_sample = 17;            //17=lite, 17=med, 20=high, 23=extreme
    //#define Volumetric_Lighting               //Disable godrays before enabling volumetric lighting.

    //#define Lens_Flares

    //#define Celshading                        //Cel shades everything, making it look somewhat like Borderlands. Zero performance impact.
        #define Celborder 1.0                   //[1.0 1.2 1.4 1.6 1.8 2.0 2.2 2.4 2.6 2.8 3.0]
	    #define Celradius 1.0                   //[1.0 0.9 0.8 0.7 0.6 0.5 0.4 0.3 0.2 0.1 0.075 0.05]

    //#define Whiteworld                        //Makes the ground white, screenshot -> https://i.imgur.com/xziUB8O.png

    #define Moonlight 0.003                     //[0.0 0.0015 0.003 0.006 0.009]

    //#define defskybox

    #define emissive_R 1.5                      //[0.0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0]
    #define emissive_G 0.42                     //[0.0 0.1 0.2 0.3 0.42 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0]
    #define emissive_B 0.045                    //[0.0 0.045 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0]
    #define minlight 0.002                      //Tweak the amount of minimal light inside caves etc. [0.001 0.002 0.003 0.004 0.005 0.006 0.007 0.008 0.009 0.010 0.011 0.012 0.013 0.014 0.015 0.016 0.017 0.018 0.019 0.020 0.021 0.022 0.023 0.024 0.025 0.026 0.027 0.028 0.029 0.030]
    
    #ifdef Penumbra
        //required for optifine to parse it since it doesn't parse #if defined penumbra, nvidia, windows.
    #endif

    //Use the same color as water for water shading, diffuse
    #define waterCR 0.0	        //[0.0 0.1 0.125 0.15 0.175 0.2 0.225 0.25 0.275 0.3 0.325 0.35 0.375 0.4 0.6 0.8 1.0 1.2 1.4 1.6 1.8 2.0 2.2 2.4 2.6 2.8 3.0]
    #define waterCG 0.175	    //[0.0 0.1 0.125 0.15 0.175 0.2 0.225 0.25 0.275 0.3 0.325 0.35 0.375 0.4 0.6 0.65 0.8 1.0 1.2 1.4 1.6 1.8 2.0 2.2 2.4 2.6 2.8 3.0]
    #define waterCB 0.2	        //[0.0 0.1 0.125 0.15 0.175 0.2 0.225 0.25 0.275 0.3 0.325 0.35 0.375 0.4 0.6 0.8 1.0 1.2 1.25 1.4 1.6 1.8 2.0 2.2 2.4 2.6 2.8 3.0]    
#endif

#ifdef composite1
	const int noiseTextureResolution = 128;     //must be in composite1 for 2d clouds
    
	#define Godrays
		#define Godrays_Density 1.15            //[0.575 1.15 2.3 4.6 9.2]
	//#define Lens_Flares

	//#define Volumetric_Lighting               //Disable godrays before enabling volumetric lighting.
	
	#define Fog                                 //Toggle fog
		#define wFogDensity	100.0               //adjust fog density [5.0 10.0 15.0 20.0 25.0 30.0 35.0 40.0 45.0 50.0 55.0 60.0 65.0 70.0 75.0 80.0 90.0 100.0 110.0 120.0 130.0 140.0 150.0 160.0 170.0 180.0 190.0 200.0 210.0 220.0 230.0 240.0 250.0 260.0 270.0 280.0 290.0 300.0]		
        //#define morningFog                    //Toggle dynamic fog during sunrise.
    #define Underwater_Fog                      //Toggle underwater fog. 
		#define uFogDensity 25.0                //adjust underwater fog density [5.0 10.0 15.0 20.0 25.0 30.0 35.0 40.0 45.0 50.0 55.0 60.0 65.0 70.0 75.0 80.0 90.0 100.0 110.0 120.0 130.0 140.0 150.0 160.0 170.0 180.0 190.0 200.0]		
		#define uwatertint                      //Tints the underwater ground with blue color.

	#define Clouds 3                            //[0 1 2 3 4] Toggle clouds. 0=Off, 1=Default MC, 2=2D, 3=VL, 4=2D+VL, also adjust in gbuffers_cloud
		#define cloudsIT 6                      //[6 8 10 12 14 16 18 20 24 32 48] Volumetric clouds quality.
		#define cloudreflIT 2                   //[2 4 6 8 10 12 14 16] Reflected volumetric clouds quality.
		//#define Cloud_reflection              //Toggle clouds reflection in water	
        #define cloud_height 256.0		        //[64.0 80.0 96.0 112.0 128.0 144.0 160.0 176.0 192.0 208.0 224.0 240.0 256.0 272.0 288.0 304.0 320.0 336.0 352.0 368.0 384.0 400.0]

	//#define waterRefl                           //Toggle water reflections
	//#define iceRefl                             //Toggle stained glass and ice reflections
	
    //#define Refraction                          //Toggle water refractions.
    //#define Caustics                            //Toggle water caustics.
    #define causticsStrength 0.8                //[0.1 0.15 0.2 0.25 0.30 0.35 0.4 0.45 0.5 0.55 0.6 0.65 0.7 0.75 0.8 0.85 0.9 0.95 1.0 1.05 1.10 1.15 1.20 1.25 1.3 1.35 1.4 1.45 1.5 1.55 1.6 1.65 1.75 1.8 1.85 1.9 1.95 2.0]
    #define waveSize 1.0                        //[0.25 0.5 0.75 1.0 1.25 1.5 1.75 2.0 2.25 2.5 2.75 3.0] Adjust water wave size, includes refraction and caustic size.

    //#define metallicRefl                        //Toggle reflections for metallic blocks defined in block.properties
    //#define polishedRefl                        //Toggle reflections for polished blocks defined in block.properties   
    #define metalStrength 1.0                   //[0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0] Metallic and polished reflection strength
    #define metallicSky 0.1                     //[0.0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0] Sky reflection strength

	//#define RainReflections                   //Toggle rain reflections, wetness
	#define BiomeCheck                          //Toggle biome check for rain reflections. Only enable if no reflections are present. Updating optifine might resolve missing reflections with this on.
	#define rainNoise 1.0                       //[1.0 2.0 3.0 4.0 5.0 6.0 7.0 8.0 9.0 10.0] Higher values means less noise

	#define customStars                         //Toggle custom stars.	

    //#define defskybox                         //Toggle support for the default skybox, including custom skies from resourcepacks. If no resourcepack is present this will only enable the default sun and moon texture. This option also auto disables the custom shader sun and moon.
    #define skyboxblendfactor 0.75              //[0.0 0.05 0.1 0.15 0.2 0.25 0.3 0.35 0.4 0.45 0.5 0.55 0.6 0.65 0.7 0.75 0.8 0.85 0.9 0.95 1.0] Adjust the blend factor of custom sky and default skybox

	#define emissive_R 1.5                      //[0.0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0]
	#define emissive_G 0.42                     //[0.0 0.1 0.2 0.3 0.42 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0]
	#define emissive_B 0.045                    //[0.0 0.045 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0]

    #ifdef Cloud_reflection
        //required for optifine to parse it since it doesn't parse #if defined
    #endif  
    #ifdef waterRefl
        //required for optifine to parse it since it doesn't parse #if defined
    #endif    
#endif

#ifdef composite2
    //#define TAA							        //Toggle temporal anti-aliasing (TAA)
    #define TAA_quality	2				        //[1 2] Fast is more blurry during movement compared to fancy.

    //#define Adaptive_sharpening			    //Toggle adaptive sharpening. Recommended to use with TAA. Disabling TAA also disables adaptive sharpening.
    #define AS_sharpening 0.5 			        //[0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0] Adjust sharpening strength.
#endif

#ifdef composite3
    //#define Bloom
#endif    

#ifdef composite4
    //#define Bloom				                //Also disables sun glare
    #define bloom_strength 0.75	                //Adjust bloom strength [0.5 0.75 1.0 2.0]
#endif

#ifdef final
    //#define Bloom

    #define Rain_Drops			                //Enables rain drops on screen during raining. Requires sun effects to be enabled. Low performance impact.

    //#define Refraction

    //#define Depth_of_Field	                //Simulates eye focusing on objects. Low performance impact
	    //#define Distance_Blur	                //Requires Depth of Field to be enabled. Replaces eye focusing effect with distance being blurred instead.
        #define smoothDof                       //Toggle smooth transition between clear and blurry.

    //#define Motionblur		                //Blurres your view/camera during movemenent. Low performance impact. Doesn't work with Depth of Field.

    #define Cloudsblur			                //Blurres the sky abit, making volumetric clouds less noisy. Doesn't work with MB and DoF
	
    //Defined values for Optifine
    #define DoF_Strength 90		                //[10 20 30 40 50 60 70 80 90 100 110 120 130 140 150 160 170 180 190 200 210 220 230 240 250 260 270 280 290 300]
    #define Dof_Distance_View 256               //[128 256 384 512]
    #define MB_strength 0.014	                //[0.008 0.014 0.020]
    #define Contrast 2.2                        //Lower values increase contrast while higher values reduce conrast. If you adjust contrast make sure adjust brightness aswell. [1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0 2.1 2.2 2.3 2.4 2.5 2.6 2.7 2.8 2.9 3.0 3.1 3.2 3.3 3.4 3.5 3.6 3.7 3.8 3.9 4.0 4.1 4.2 4.3 4.4 4.5 4.6 4.7 4.8 4.9 5.0] 
	#define Brightness 1.0                      //Adjust brightness. [0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0 2.1 2.2 2.3 2.4 2.5 2.6 2.7 2.8 2.9 3.0 3.1 3.2 3.3 3.4 3.5 3.6 3.7 3.8 3.9 4.0 4.1 4.2 4.3 4.4 4.5 4.6 4.7 4.8 4.9 5.0]

    #define Showbuffer 0	                    //[0 1 2 25 3 35 4 5 6 7 8]
#endif
 
#ifdef gbuffers_skytextured
    //#define defskybox
#endif

#ifdef gbuffers_terrain
    #define nMap 0				//[0 1 2]0=Off 1=Bumpmapping, 2=Parallax
    #define POM_RES 32			//Texture / Resourcepack resolution. [32 64 128 256 512 1024 2048]
    #define POM_DIST 16.0		//[8.0 16.0 24.0 32.0 40.0 48.0 56.0 64.0 72.0 80.0 88.0 96.0 104.0 112.0 120.0 128.0]
    #define POM_DEPTH 0.30		//[0.05 0.10 0.15 0.20 0.25 0.30 0.35 0.40 0.45 0.50 0.55 0.60 0.65 0.70 0.75 0.80 0.85 0.90 0.95 1.0]

    //#define metallicRefl        //Toggle reflections for blocks defined in block.properties
    //#define polishedRefl        //Toggle reflections for polished blocks defined in block.properties

    #define Waving_Leaves
    #define Waving_Vines
    #define Waving_Grass		//Does not include tallgrass due some issues with it.
    #define Waving_Tallgrass
    #define Waving_Fire
    #define Waving_Lava
    #define Waving_Lilypads
    #define Waving_Lanterns   
    #define Waving_Entities		//Includes: Saplings, small flowers, wheat, carrots, potatoes and beetroot.
    #define animationSpeed 1.0  //[0.25 0.5 0.75 1.0 1.25 1.5 1.75 2.0]
#endif

#ifdef gbuffers_texturedblock   //textured and block are mostly the same so lets use the same defines
    #define MobsFlashRed
    #define emissive_R 1.5		//[0.0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0]
    #define emissive_G 0.42		//[0.0 0.1 0.2 0.3 0.42 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0]
    #define emissive_B 0.045	//[0.0 0.045 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0]
    #define minlight 0.002      //Tweak the amount of minimal light inside caves etc. [0.001 0.002 0.003 0.004 0.005 0.006 0.007 0.008 0.009 0.010 0.011 0.012 0.013 0.014 0.015 0.016 0.017 0.018 0.019 0.020 0.021 0.022 0.023 0.024 0.025 0.026 0.027 0.028 0.029 0.030]
#endif

#ifdef gbuffers_water
    #define Waving_Water
    #define waves_amplitude 0.65    //[0.55 0.65 0.75 0.85 0.95 1.05 1.15 1.25 1.35 1.45 1.55 1.65 1.75 1.85 1.95 2.05]
    #define waveSize 1.0            //[0.25 0.5 0.75 1.0 1.25 1.5 1.75 2.0 2.25 2.5 2.75 3.0] Adjust water wave size, includes refraction and caustic size.

    //#define watertex
    #define wtexblend 0.12      //[0.02 0.03 0.04 0.05 0.06 0.07 0.08 0.09 0.10 0.11 0.12 0.13 0.14 0.15 0.16 0.17 0.18 0.19 0.20 0.21 0.22 0.23 0.24 0.25 0.26 0.27 0.28 0.29 0.30 0.31 0.32 0.33 0.34 0.35 0.36 0.37 0.38 0.39 0.40 0.41 0.42 0.43 0.44 0.45 0.46 0.47 0.48 0.49 0.50]
    //#define WaterParallax
    #define waterheight 1.0     //[1.0 1.5 2.0] height for parallax mapping

    #define waterCR 0.0	        //[0.0 0.1 0.125 0.15 0.175 0.2 0.225 0.25 0.275 0.3 0.325 0.35 0.375 0.4 0.6 0.8 1.0 1.2 1.4 1.6 1.8 2.0 2.2 2.4 2.6 2.8 3.0]
    #define waterCG 0.175	    //[0.0 0.1 0.125 0.15 0.175 0.2 0.225 0.25 0.275 0.3 0.325 0.35 0.375 0.4 0.6 0.65 0.8 1.0 1.2 1.4 1.6 1.8 2.0 2.2 2.4 2.6 2.8 3.0]
    #define waterCB 0.2	        //[0.0 0.1 0.125 0.15 0.175 0.2 0.225 0.25 0.275 0.3 0.325 0.35 0.375 0.4 0.6 0.8 1.0 1.2 1.25 1.4 1.6 1.8 2.0 2.2 2.4 2.6 2.8 3.0]
    #define waterA 6.0	        //[1.0 1.5 2.0 2.5 3.0 3.5 4.0 4.5 5.0 5.5 6.0 7.0 8.0 9.0 10.0]

    #define emissive_R 1.5		//[0.0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0]
    #define emissive_G 0.42		//[0.0 0.1 0.2 0.3 0.42 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0]
    #define emissive_B 0.045	//[0.0 0.045 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0]
#endif

#ifdef gbuffers_weather
    //#define WeatherAngle		//Toggle alternative weather angle
#endif

#ifdef lightingColors
	#define light_brightness 1.0 //[0.01 0.02 0.03 0.04 0.05 0.06 0.07 0.08 0.09 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0]
    #define r_multiplier 0.0	//[0.0 0.25 0.5 0.75 1.0 1.25 1.5 1.75 2.0 2.25 2.5 2.75 3.0 3.25 3.5 3.75 4.0 4.25 4.5 4.75 5.0 5.25 5.5 5.75 6.0 6.25 6.5 6.75 7.0 7.25 7.5 7.75 8.0 8.25 8.5 8.75 9.0 9.25 9.5 9.75 10.0]
	#define g_multiplier 0.0	//[0.0 0.25 0.5 0.75 1.0 1.25 1.5 1.75 2.0 2.25 2.5 2.75 3.0 3.25 3.5 3.75 4.0 4.25 4.5 4.75 5.0 5.25 5.5 5.75 6.0 6.25 6.5 6.75 7.0 7.25 7.5 7.75 8.0 8.25 8.5 8.75 9.0 9.25 9.5 9.75 10.0]
	#define b_multiplier 0.0	//[0.0 0.25 0.5 0.75 1.0 1.25 1.5 1.75 2.0 2.25 2.5 2.75 3.0 3.25 3.5 3.75 4.0 4.25 4.5 4.75 5.0 5.25 5.5 5.75 6.0 6.25 6.5 6.75 7.0 7.25 7.5 7.75 8.0 8.25 8.5 8.75 9.0 9.25 9.5 9.75 10.0]
#endif

varying vec2 texcoord;
varying vec3 lightColor;
varying vec3 sunVec;
varying vec3 upVec;
varying vec3 sky1;
varying vec3 sky2;

varying float tr;

varying vec2 lightPos;

varying vec3 sunlight;
varying vec3 nsunlight;

varying vec3 rawAvg;

varying float SdotU;
varying float sunVisibility;
varying float moonVisibility;

uniform vec3 sunPosition;
uniform vec3 upPosition;
uniform int worldTime;
uniform float rainStrength;
uniform mat4 gbufferProjection;

const vec3 ToD[7] = vec3[7](  vec3(0.58597,0.16,0.005),
								vec3(0.58597,0.31,0.08),
								vec3(0.58597,0.45,0.16),
								vec3(0.58597,0.5,0.35),
								vec3(0.58597,0.5,0.36),
								vec3(0.58597,0.5,0.37),
								vec3(0.58597,0.5,0.38));

void main() {
	//lightpos
	vec4 tpos = vec4(sunPosition,1.0)*gbufferProjection;
	tpos = vec4(tpos.xyz/tpos.w,1.0);
	vec2 pos1 = tpos.xy/tpos.z;
	lightPos = pos1*0.5+0.5;
	/*----------------------------*/
	
	//Position
	gl_Position = ftransform();
	texcoord = (gl_MultiTexCoord0).xy;
	/*--------------------------------*/

	//Sun/moon pos
	sunVec = normalize(sunPosition);
	upVec = normalize(upPosition);

	SdotU = dot(sunVec,upVec);
	sunVisibility = pow(clamp(SdotU+0.15,0.0,0.15)/0.15,4.0);
	moonVisibility = pow(clamp(-SdotU+0.15,0.0,0.15)/0.15,4.0);
	/*--------------------------------*/

	//reduced the sun color to a 7 array
	float hour = max(mod(worldTime/1000.0+2.0,24.0)-2.0,0.0);  //-0.1
	float cmpH = max(-abs(floor(hour)-6.0)+6.0,0.0); //12
	float cmpH1 = max(-abs(floor(hour)-5.0)+6.0,0.0); //1

	vec3 temp = ToD[int(cmpH)];
	vec3 temp2 = ToD[int(cmpH1)];

	sunlight = mix(temp,temp2,fract(hour));
	sunlight.rgb += vec3(r_multiplier,g_multiplier,b_multiplier); //allows lighting colors to be tweaked.
	sunlight.rgb *= light_brightness; //brightness needs to be adjusted if we tweak lighting colors.

	vec2 trCalc = min(abs(worldTime-vec2(23000.0,12700.0)),750.0);
	tr = max(min(trCalc.x,trCalc.y)/375.0-1.0,0.0);	
	/*--------------------------------*/
	float tr2 = clamp(min(min(distance(float(worldTime),23000.0),750.0),min(distance(float(worldTime),12700.0),800.0))/800.0-0.5,0.0,1.0)*2.0;

	vec4 bounced = vec4(0.5,0.66,1.3,0.27);
	vec3 sun_ambient = bounced.w * (vec3(0.25,0.62,1.32)-rainStrength*vec3(0.1,0.47,1.17))*(1.0+rainStrength*7.0) + sunlight*(bounced.x + bounced.z)*(1.0-rainStrength*0.95);

	const vec3 moonlight = vec3(0.0016, 0.00288, 0.00448);
	rawAvg = (sun_ambient*sunVisibility + 8.0*moonlight*moonVisibility)*(0.05+tr2*0.15)*4.7+0.0002;

	float truepos = sign(sunPosition.z)*1.0;		//1 -> sun / -1 -> moon
	lightColor = mix(vec3(Lens_Flares_Strength)*sunVisibility+0.00001,12.*moonlight*moonVisibility+0.00001,(truepos+1.0)/2.);
	if (length(lightColor)>0.001)lightColor = mix(lightColor,normalize(vec3(0.3,0.3,0.3))*pow(normalize(lightColor),vec3(0.4))*length(lightColor)*0.03,rainStrength)*(0.25+0.25*tr2);

	float mcosS = max(SdotU,0.0);
	vec3 sunlight04 = pow(sunlight,vec3(0.454));
	
	float skyMult = max(SdotU*0.1+0.1,0.0)/0.2*(1.0-rainStrength*0.6)*0.7;
	nsunlight = normalize(pow(mix(sunlight04,5.*sunlight04*sunVisibility*(1.0-rainStrength*0.95)+vec3(0.3,0.3,0.35),rainStrength),vec3(2.2)))*0.6*skyMult;

	vec3 sky_color = vec3(0.05, 0.32, 1.);
	sky_color = normalize(mix(sky_color,2.*sunlight04*sunVisibility*(1.0-rainStrength*0.95)+vec3(0.3,0.3,0.3)*length(sunlight04),rainStrength)); //normalize colors in order to don't change luminance

	sky1 = sky_color*0.6*skyMult;
	sky2 = mix(sky_color,mix(nsunlight,sky_color,rainStrength*0.9),1.0-max(mcosS-0.2,0.0)*0.5)*0.6*skyMult;
	
}

