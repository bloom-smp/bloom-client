#version 150 core
#define gl_FragData iris_FragData
#define varying in
#define gl_ModelViewProjectionMatrix (gl_ProjectionMatrix * gl_ModelViewMatrix)
#define gl_ModelViewMatrix mat4(1.0)
#define gl_NormalMatrix mat3(1.0)
#define gl_Color vec4(1.0, 1.0, 1.0, 1.0)
#define gl_ProjectionMatrix mat4(1.0)
#define gl_FogFragCoord iris_FogFragCoord
#extension GL_ARB_shader_texture_lod : enable
uniform float iris_FogDensity;
uniform float iris_FogStart;
uniform float iris_FogEnd;
uniform vec4 iris_FogColor;

struct iris_FogParameters {
    vec4 color;
    float density;
    float start;
    float end;
    float scale;
};

iris_FogParameters iris_Fog = iris_FogParameters(iris_FogColor, iris_FogDensity, iris_FogStart, iris_FogEnd, 1.0 / (iris_FogEnd - iris_FogStart));

#define gl_Fog iris_Fog
in float iris_FogFragCoord;
out vec4 iris_FragData[8];
vec4 texture2D(sampler2D sampler, vec2 coord) { return texture(sampler, coord); }
vec4 texture2D(sampler2D sampler, vec2 coord, float bias) { return texture(sampler, coord, bias); }
vec4 texture2DLod(sampler2D sampler, vec2 coord, float lod) { return textureLod(sampler, coord, lod); }
vec4 shadow2D(sampler2DShadow sampler, vec3 coord) { return vec4(texture(sampler, coord)); }
vec4 shadow2DLod(sampler2DShadow sampler, vec3 coord, float lod) { return vec4(textureLod(sampler, coord, lod)); }
const bool gaux1MipmapEnabled = true;
/* DRAWBUFFERS:3 */

#define composite2
#define composite1
/*
Thank you for downloading Sildur's vibrant shaders, make sure you got it from the official source found here:
https://sildurs-shaders.github.io/
*/
#ifdef gbuffers_shadows
    #define Shadows								//Toggle all shadows
    #define SHADOW_MAP_BIAS 0.80
    #define grass_shadows                       //Also disables tallgrass and flowers shadows
    #ifdef Shadows
    const float shadowDistance = 70.0;			//Render distance of shadows. 60=lite, 80=med, 80=high, 120=extreme [60.0 70.0 80.0 90.0 100.0 110.0 120.0 130.0 140.0 150.0 160.0 170.0 180.0 190.0 200.0 210.0 220.0 230.0 240.0 250.0 260.0 270.0 280.0 290.0 300.0 310.0 320.0 330.0 340.0 350.0 360.0 370.0 380.0 390.0 400.0]
    const int shadowMapResolution = 512;		//Shadows resolution. [256 512 1024 2048 3072 4096 6144 8192 16384] 512=lite, 1024=med, 2048=high, 3072=extreme 
    const float k = 1.8;
    #define Nearshadowplane 0.05	            //[0.04 0.045 0.05 0.055 0.06 0.065 0.07 0.075 0.08 0.085 0.09 0.095 0.1] close quality, lower=higher quality, 0.09 is required for 2.0 farshadowmap to prevent glitches
    #define Farshadowplane 1.4                  //[0.6 0.8 1.0 1.2 1.4 1.6 1.8 2.0] far quality, streches the shadowmap, lower values closer, higher further away: 0.8=extreme, 1.0=high, 1.2=medium, 1.4=lite
    float a = exp(Nearshadowplane);
    float b = (exp(Farshadowplane)-a)*shadowDistance/128.0;
    float calcDistortion(vec2 worldpos){
        return 1.0/(log(length(worldpos)*b+a)*k);
    }
    #endif
#endif

#ifdef composite0
    #define ColoredShadows						//Toggle colored shadows
    //#define Penumbra                            //Toggle penumbra soft shadows
    //#define raytracedShadows                  //Improves closeup and faraway shadows. Also allows shadows to be cast outside of the shadowmap, outside of your shadows render distance. Requires shadows to be enabled. Has some issues since it's raytraced in screenspace.
    const int VPS_samples = 8;                  //Used for penumbra shadows [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30]
    #define shadow_samples 10                   //Used for shadows in general [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30]
    const bool 	shadowHardwareFiltering0 = true;
    const bool 	shadowHardwareFiltering1 = true;
    const float	sunPathRotation	= -40.0;		//[-10.0 -20.0 -30.0 -40.0 -50.0 -60.0 -70.0 -80.0 0.0 10.0 20.0 30.0 40.0 50.0 60.0 70.0 80.0]
    
    //#define SSDO				                //Ambient Occlusion, makes lighting more realistic. High performance impact.
        #define ao_strength 3.0                 //[1.0 2.0 3.0 4.0 5.0 6.0 7.0 8.0 9.0 10.0]

    #define Godrays
        const int grays_sample = 17;            //17=lite, 17=med, 20=high, 23=extreme
    //#define Volumetric_Lighting               //Disable godrays before enabling volumetric lighting.

    //#define Lens_Flares

    //#define Celshading                        //Cel shades everything, making it look somewhat like Borderlands. Zero performance impact.
        #define Celborder 1.0                   //[1.0 1.2 1.4 1.6 1.8 2.0 2.2 2.4 2.6 2.8 3.0]
	    #define Celradius 1.0                   //[1.0 0.9 0.8 0.7 0.6 0.5 0.4 0.3 0.2 0.1 0.075 0.05]

    //#define Whiteworld                        //Makes the ground white, screenshot -> https://i.imgur.com/xziUB8O.png

    #define Moonlight 0.003                     //[0.0 0.0015 0.003 0.006 0.009]

    //#define defskybox

    #define emissive_R 1.5                      //[0.0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0]
    #define emissive_G 0.42                     //[0.0 0.1 0.2 0.3 0.42 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0]
    #define emissive_B 0.045                    //[0.0 0.045 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0]
    #define minlight 0.002                      //Tweak the amount of minimal light inside caves etc. [0.001 0.002 0.003 0.004 0.005 0.006 0.007 0.008 0.009 0.010 0.011 0.012 0.013 0.014 0.015 0.016 0.017 0.018 0.019 0.020 0.021 0.022 0.023 0.024 0.025 0.026 0.027 0.028 0.029 0.030]
    
    #ifdef Penumbra
        //required for optifine to parse it since it doesn't parse #if defined penumbra, nvidia, windows.
    #endif

    //Use the same color as water for water shading, diffuse
    #define waterCR 0.0	        //[0.0 0.1 0.125 0.15 0.175 0.2 0.225 0.25 0.275 0.3 0.325 0.35 0.375 0.4 0.6 0.8 1.0 1.2 1.4 1.6 1.8 2.0 2.2 2.4 2.6 2.8 3.0]
    #define waterCG 0.175	    //[0.0 0.1 0.125 0.15 0.175 0.2 0.225 0.25 0.275 0.3 0.325 0.35 0.375 0.4 0.6 0.65 0.8 1.0 1.2 1.4 1.6 1.8 2.0 2.2 2.4 2.6 2.8 3.0]
    #define waterCB 0.2	        //[0.0 0.1 0.125 0.15 0.175 0.2 0.225 0.25 0.275 0.3 0.325 0.35 0.375 0.4 0.6 0.8 1.0 1.2 1.25 1.4 1.6 1.8 2.0 2.2 2.4 2.6 2.8 3.0]    
#endif

#ifdef composite1
	const int noiseTextureResolution = 128;     //must be in composite1 for 2d clouds
    
	#define Godrays
		#define Godrays_Density 1.15            //[0.575 1.15 2.3 4.6 9.2]
	//#define Lens_Flares

	//#define Volumetric_Lighting               //Disable godrays before enabling volumetric lighting.
	
	#define Fog                                 //Toggle fog
		#define wFogDensity	100.0               //adjust fog density [5.0 10.0 15.0 20.0 25.0 30.0 35.0 40.0 45.0 50.0 55.0 60.0 65.0 70.0 75.0 80.0 90.0 100.0 110.0 120.0 130.0 140.0 150.0 160.0 170.0 180.0 190.0 200.0 210.0 220.0 230.0 240.0 250.0 260.0 270.0 280.0 290.0 300.0]		
        //#define morningFog                    //Toggle dynamic fog during sunrise.
    #define Underwater_Fog                      //Toggle underwater fog. 
		#define uFogDensity 25.0                //adjust underwater fog density [5.0 10.0 15.0 20.0 25.0 30.0 35.0 40.0 45.0 50.0 55.0 60.0 65.0 70.0 75.0 80.0 90.0 100.0 110.0 120.0 130.0 140.0 150.0 160.0 170.0 180.0 190.0 200.0]		
		#define uwatertint                      //Tints the underwater ground with blue color.

	#define Clouds 3                            //[0 1 2 3 4] Toggle clouds. 0=Off, 1=Default MC, 2=2D, 3=VL, 4=2D+VL, also adjust in gbuffers_cloud
		#define cloudsIT 6                      //[6 8 10 12 14 16 18 20 24 32 48] Volumetric clouds quality.
		#define cloudreflIT 2                   //[2 4 6 8 10 12 14 16] Reflected volumetric clouds quality.
		//#define Cloud_reflection              //Toggle clouds reflection in water	
        #define cloud_height 256.0		        //[64.0 80.0 96.0 112.0 128.0 144.0 160.0 176.0 192.0 208.0 224.0 240.0 256.0 272.0 288.0 304.0 320.0 336.0 352.0 368.0 384.0 400.0]

	//#define waterRefl                           //Toggle water reflections
	//#define iceRefl                             //Toggle stained glass and ice reflections
	
    //#define Refraction                          //Toggle water refractions.
    //#define Caustics                            //Toggle water caustics.
    #define causticsStrength 0.8                //[0.1 0.15 0.2 0.25 0.30 0.35 0.4 0.45 0.5 0.55 0.6 0.65 0.7 0.75 0.8 0.85 0.9 0.95 1.0 1.05 1.10 1.15 1.20 1.25 1.3 1.35 1.4 1.45 1.5 1.55 1.6 1.65 1.75 1.8 1.85 1.9 1.95 2.0]
    #define waveSize 1.0                        //[0.25 0.5 0.75 1.0 1.25 1.5 1.75 2.0 2.25 2.5 2.75 3.0] Adjust water wave size, includes refraction and caustic size.

    //#define metallicRefl                        //Toggle reflections for metallic blocks defined in block.properties
    //#define polishedRefl                        //Toggle reflections for polished blocks defined in block.properties   
    #define metalStrength 1.0                   //[0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0] Metallic and polished reflection strength
    #define metallicSky 0.1                     //[0.0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0] Sky reflection strength

	//#define RainReflections                   //Toggle rain reflections, wetness
	#define BiomeCheck                          //Toggle biome check for rain reflections. Only enable if no reflections are present. Updating optifine might resolve missing reflections with this on.
	#define rainNoise 1.0                       //[1.0 2.0 3.0 4.0 5.0 6.0 7.0 8.0 9.0 10.0] Higher values means less noise

	#define customStars                         //Toggle custom stars.	

    //#define defskybox                         //Toggle support for the default skybox, including custom skies from resourcepacks. If no resourcepack is present this will only enable the default sun and moon texture. This option also auto disables the custom shader sun and moon.
    #define skyboxblendfactor 0.75              //[0.0 0.05 0.1 0.15 0.2 0.25 0.3 0.35 0.4 0.45 0.5 0.55 0.6 0.65 0.7 0.75 0.8 0.85 0.9 0.95 1.0] Adjust the blend factor of custom sky and default skybox

	#define emissive_R 1.5                      //[0.0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0]
	#define emissive_G 0.42                     //[0.0 0.1 0.2 0.3 0.42 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0]
	#define emissive_B 0.045                    //[0.0 0.045 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0]

    #ifdef Cloud_reflection
        //required for optifine to parse it since it doesn't parse #if defined
    #endif  
    #ifdef waterRefl
        //required for optifine to parse it since it doesn't parse #if defined
    #endif    
#endif

#ifdef composite2
    //#define TAA							        //Toggle temporal anti-aliasing (TAA)
    #define TAA_quality	2				        //[1 2] Fast is more blurry during movement compared to fancy.

    //#define Adaptive_sharpening			    //Toggle adaptive sharpening. Recommended to use with TAA. Disabling TAA also disables adaptive sharpening.
    #define AS_sharpening 0.5 			        //[0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0] Adjust sharpening strength.
#endif

#ifdef composite3
    //#define Bloom
#endif    

#ifdef composite4
    //#define Bloom				                //Also disables sun glare
    #define bloom_strength 0.75	                //Adjust bloom strength [0.5 0.75 1.0 2.0]
#endif

#ifdef final
    //#define Bloom

    #define Rain_Drops			                //Enables rain drops on screen during raining. Requires sun effects to be enabled. Low performance impact.

    //#define Refraction

    //#define Depth_of_Field	                //Simulates eye focusing on objects. Low performance impact
	    //#define Distance_Blur	                //Requires Depth of Field to be enabled. Replaces eye focusing effect with distance being blurred instead.
        #define smoothDof                       //Toggle smooth transition between clear and blurry.

    //#define Motionblur		                //Blurres your view/camera during movemenent. Low performance impact. Doesn't work with Depth of Field.

    #define Cloudsblur			                //Blurres the sky abit, making volumetric clouds less noisy. Doesn't work with MB and DoF
	
    //Defined values for Optifine
    #define DoF_Strength 90		                //[10 20 30 40 50 60 70 80 90 100 110 120 130 140 150 160 170 180 190 200 210 220 230 240 250 260 270 280 290 300]
    #define Dof_Distance_View 256               //[128 256 384 512]
    #define MB_strength 0.014	                //[0.008 0.014 0.020]
    #define Contrast 2.2                        //Lower values increase contrast while higher values reduce conrast. If you adjust contrast make sure adjust brightness aswell. [1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0 2.1 2.2 2.3 2.4 2.5 2.6 2.7 2.8 2.9 3.0 3.1 3.2 3.3 3.4 3.5 3.6 3.7 3.8 3.9 4.0 4.1 4.2 4.3 4.4 4.5 4.6 4.7 4.8 4.9 5.0] 
	#define Brightness 1.0                      //Adjust brightness. [0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0 2.1 2.2 2.3 2.4 2.5 2.6 2.7 2.8 2.9 3.0 3.1 3.2 3.3 3.4 3.5 3.6 3.7 3.8 3.9 4.0 4.1 4.2 4.3 4.4 4.5 4.6 4.7 4.8 4.9 5.0]

    #define Showbuffer 0	                    //[0 1 2 25 3 35 4 5 6 7 8]
#endif
 
#ifdef gbuffers_skytextured
    //#define defskybox
#endif

#ifdef gbuffers_terrain
    #define nMap 0				//[0 1 2]0=Off 1=Bumpmapping, 2=Parallax
    #define POM_RES 32			//Texture / Resourcepack resolution. [32 64 128 256 512 1024 2048]
    #define POM_DIST 16.0		//[8.0 16.0 24.0 32.0 40.0 48.0 56.0 64.0 72.0 80.0 88.0 96.0 104.0 112.0 120.0 128.0]
    #define POM_DEPTH 0.30		//[0.05 0.10 0.15 0.20 0.25 0.30 0.35 0.40 0.45 0.50 0.55 0.60 0.65 0.70 0.75 0.80 0.85 0.90 0.95 1.0]

    //#define metallicRefl        //Toggle reflections for blocks defined in block.properties
    //#define polishedRefl        //Toggle reflections for polished blocks defined in block.properties

    #define Waving_Leaves
    #define Waving_Vines
    #define Waving_Grass		//Does not include tallgrass due some issues with it.
    #define Waving_Tallgrass
    #define Waving_Fire
    #define Waving_Lava
    #define Waving_Lilypads
    #define Waving_Lanterns   
    #define Waving_Entities		//Includes: Saplings, small flowers, wheat, carrots, potatoes and beetroot.
    #define animationSpeed 1.0  //[0.25 0.5 0.75 1.0 1.25 1.5 1.75 2.0]
#endif

#ifdef gbuffers_texturedblock   //textured and block are mostly the same so lets use the same defines
    #define MobsFlashRed
    #define emissive_R 1.5		//[0.0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0]
    #define emissive_G 0.42		//[0.0 0.1 0.2 0.3 0.42 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0]
    #define emissive_B 0.045	//[0.0 0.045 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0]
    #define minlight 0.002      //Tweak the amount of minimal light inside caves etc. [0.001 0.002 0.003 0.004 0.005 0.006 0.007 0.008 0.009 0.010 0.011 0.012 0.013 0.014 0.015 0.016 0.017 0.018 0.019 0.020 0.021 0.022 0.023 0.024 0.025 0.026 0.027 0.028 0.029 0.030]
#endif

#ifdef gbuffers_water
    #define Waving_Water
    #define waves_amplitude 0.65    //[0.55 0.65 0.75 0.85 0.95 1.05 1.15 1.25 1.35 1.45 1.55 1.65 1.75 1.85 1.95 2.05]
    #define waveSize 1.0            //[0.25 0.5 0.75 1.0 1.25 1.5 1.75 2.0 2.25 2.5 2.75 3.0] Adjust water wave size, includes refraction and caustic size.

    //#define watertex
    #define wtexblend 0.12      //[0.02 0.03 0.04 0.05 0.06 0.07 0.08 0.09 0.10 0.11 0.12 0.13 0.14 0.15 0.16 0.17 0.18 0.19 0.20 0.21 0.22 0.23 0.24 0.25 0.26 0.27 0.28 0.29 0.30 0.31 0.32 0.33 0.34 0.35 0.36 0.37 0.38 0.39 0.40 0.41 0.42 0.43 0.44 0.45 0.46 0.47 0.48 0.49 0.50]
    //#define WaterParallax
    #define waterheight 1.0     //[1.0 1.5 2.0] height for parallax mapping

    #define waterCR 0.0	        //[0.0 0.1 0.125 0.15 0.175 0.2 0.225 0.25 0.275 0.3 0.325 0.35 0.375 0.4 0.6 0.8 1.0 1.2 1.4 1.6 1.8 2.0 2.2 2.4 2.6 2.8 3.0]
    #define waterCG 0.175	    //[0.0 0.1 0.125 0.15 0.175 0.2 0.225 0.25 0.275 0.3 0.325 0.35 0.375 0.4 0.6 0.65 0.8 1.0 1.2 1.4 1.6 1.8 2.0 2.2 2.4 2.6 2.8 3.0]
    #define waterCB 0.2	        //[0.0 0.1 0.125 0.15 0.175 0.2 0.225 0.25 0.275 0.3 0.325 0.35 0.375 0.4 0.6 0.8 1.0 1.2 1.25 1.4 1.6 1.8 2.0 2.2 2.4 2.6 2.8 3.0]
    #define waterA 6.0	        //[1.0 1.5 2.0 2.5 3.0 3.5 4.0 4.5 5.0 5.5 6.0 7.0 8.0 9.0 10.0]

    #define emissive_R 1.5		//[0.0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0]
    #define emissive_G 0.42		//[0.0 0.1 0.2 0.3 0.42 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0]
    #define emissive_B 0.045	//[0.0 0.045 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0]
#endif

#ifdef gbuffers_weather
    //#define WeatherAngle		//Toggle alternative weather angle
#endif

#ifdef lightingColors
	#define light_brightness 1.0 //[0.01 0.02 0.03 0.04 0.05 0.06 0.07 0.08 0.09 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0]
    #define r_multiplier 0.0	//[0.0 0.25 0.5 0.75 1.0 1.25 1.5 1.75 2.0 2.25 2.5 2.75 3.0 3.25 3.5 3.75 4.0 4.25 4.5 4.75 5.0 5.25 5.5 5.75 6.0 6.25 6.5 6.75 7.0 7.25 7.5 7.75 8.0 8.25 8.5 8.75 9.0 9.25 9.5 9.75 10.0]
	#define g_multiplier 0.0	//[0.0 0.25 0.5 0.75 1.0 1.25 1.5 1.75 2.0 2.25 2.5 2.75 3.0 3.25 3.5 3.75 4.0 4.25 4.5 4.75 5.0 5.25 5.5 5.75 6.0 6.25 6.5 6.75 7.0 7.25 7.5 7.75 8.0 8.25 8.5 8.75 9.0 9.25 9.5 9.75 10.0]
	#define b_multiplier 0.0	//[0.0 0.25 0.5 0.75 1.0 1.25 1.5 1.75 2.0 2.25 2.5 2.75 3.0 3.25 3.5 3.75 4.0 4.25 4.5 4.75 5.0 5.25 5.5 5.75 6.0 6.25 6.5 6.75 7.0 7.25 7.5 7.75 8.0 8.25 8.5 8.75 9.0 9.25 9.5 9.75 10.0]
#endif

//custom uniforms defined in shaders.properties
uniform float inSwamp;
uniform float BiomeTemp;

varying vec2 texcoord;
varying vec2 lightPos;

varying vec3 sunVec;
varying vec3 upVec;
varying vec3 lightColor;
varying vec3 sky1;
varying vec3 sky2;
varying vec3 nsunlight;
varying vec3 sunlight;
const vec3 moonlight = vec3(0.0025, 0.0045, 0.007);
varying vec3 rawAvg;
varying vec3 avgAmbient2;
varying vec3 cloudColor;
varying vec3 cloudColor2;

varying float fading;
varying float tr;
varying float eyeAdapt;
varying float SdotU;
varying float sunVisibility;
varying float moonVisibility;

uniform sampler2D composite;
uniform sampler2D gaux1;
uniform sampler2D depthtex0;
uniform sampler2D depthtex1;
uniform sampler2D depthtex2;
uniform sampler2D colortex0;
uniform sampler2D colortex1;
uniform sampler2D colortex2;

uniform sampler2D noisetex;
uniform sampler2D gaux3;
uniform sampler2D gaux2;
uniform sampler2D gaux4;

uniform vec3 cameraPosition;
uniform vec3 sunPosition;

uniform mat4 gbufferProjection;
uniform mat4 gbufferProjectionInverse;
uniform mat4 gbufferModelViewInverse;

uniform ivec2 eyeBrightness;
uniform ivec2 eyeBrightnessSmooth;
uniform int isEyeInWater;
uniform int worldTime;
uniform float near;
uniform float far;
uniform float viewWidth;
uniform float viewHeight;
uniform float rainStrength;
uniform float frameTimeCounter;
uniform float blindness;

/*------------------------------------------*/
float comp = 1.0-near/far/far;
float tmult = mix(min(abs(worldTime-6000.0)/6000.0,1.0),1.0,rainStrength);
float night = clamp((worldTime-13000.0)/300.0,0.0,1.0)-clamp((worldTime-22800.0)/200.0,0.0,1.0);

//Draw sun and moon
vec3 drawSun(vec3 fposition, vec3 color, float vis) {
	vec3 sVector = normalize(fposition);
	float angle = (1.0-max(dot(sVector,sunVec),0.0))* 650.0;
	float sun = exp(-angle*angle*angle);
			sun *= (1.0-rainStrength*1.0)*sunVisibility;

	vec3 sunlightB = mix(pow(sunlight,vec3(1.0))*44.0,vec3(0.25,0.3,0.4),rainStrength*0.8);
if(isEyeInWater	== 1.0)sunlightB = mix(pow(moonlight*40.0,vec3(1.0))*44.0,vec3(0.25,0.3,0.4),rainStrength*0.8);	//lets just render the moon underwater, it has the perfect color

	return mix(color,sunlightB,sun*vis);
}
vec3 drawMoon(vec3 fposition, vec3 color, float vis) {
	vec3 sVector = normalize(fposition);
	float angle = (1.0-max(dot(sVector,-sunVec),0.0))* 2000.0;
	float moon = exp(-angle*angle*angle);
			moon *= (1.0-rainStrength*1.0)*moonVisibility;
	vec3 moonlightC = mix(pow(moonlight*40.0,vec3(1.0))*44.0,vec3(0.25,0.3,0.4),rainStrength*0.8);

	return mix(color,moonlightC,moon*vis);
}/**--------------------------------------*/

#ifdef Fog
float getAirDensity (float h) {
	return max(h/10.,6.0);
}

float calcFog(vec3 fposition) {
	float density = wFogDensity*(1.0-rainStrength*0.115);

#ifdef morningFog
	float morning = clamp((worldTime-0.1)/300.0,0.0,1.0)-clamp((worldTime-23150.0)/200.0,0.0,1.0);
	density *= (0.1+0.9*morning);
#endif
	vec3 worldpos = (gbufferModelViewInverse*vec4(fposition,1.0)).rgb+cameraPosition;
	float height = mix(getAirDensity (worldpos.y),6.,rainStrength);
	float d = length(fposition);

	return pow(clamp((2.625+rainStrength*3.4)/exp(-60/10./density)*exp(-getAirDensity (cameraPosition.y)/density) * (1.0-exp( -pow(d,2.712)*height/density/(6000.-tmult*tmult*2000.)/13))/height,0.0,1.),1.0-rainStrength*0.63)*clamp((eyeBrightnessSmooth.y/255.-2/16.)*4.,0.0,1.0);
}
#else
float calcFog(vec3 fposition) {
	return 0.0;
}
#endif

//Skycolor
vec3 getSkyc(vec3 fposition) {
vec3 sVector = normalize(fposition);

float invRain07 = 1.0-rainStrength*0.6;
float cosT = dot(sVector,upVec);
float mCosT = max(cosT,0.0);
float absCosT = 1.0-max(cosT*0.82+0.26,0.2);
float cosY = dot(sunVec,sVector);
float Y = acos(cosY);

const float a = -1.;
const float b = -0.22;
const float c = 8.0;
const float d = -3.5;
const float e = 0.3;

//luminance
float L =  (1.0+a*exp(b/(mCosT)));
float A = 1.0+e*cosY*cosY;

//gradient
vec3 grad1 = mix(sky1,sky2,absCosT*absCosT);
float sunscat = max(cosY,0.0);
vec3 grad3 = mix(grad1,nsunlight,sunscat*sunscat*(1.0-mCosT)*(0.9-rainStrength*0.5*0.9)*(clamp(-(SdotU)*4.0+3.0,0.0,1.0)*0.65+0.35)+0.1);

float Y2 = 3.14159265359-Y;
float L2 = L * (8.0*exp(d*Y2)+A);

const vec3 moonlight2 = pow(normalize(moonlight),vec3(3.0))*length(moonlight);
const vec3 moonlightRain = normalize(vec3(0.25,0.3,0.4))*length(moonlight);
vec3 gradN = mix(moonlight,moonlight2,1.-L2/2.0);
gradN = mix(gradN,moonlightRain,rainStrength);

return pow(L*(c*exp(d*Y)+A),invRain07)*sunVisibility *length(rawAvg) * (0.85+rainStrength*0.425)*grad3+ 0.2*pow(L2*1.2+1.2,invRain07)*moonVisibility*gradN;
}/*---------------------------------*/

#if defined waterRefl || defined iceRefl ||defined metallicRefl || defined polishedRefl || defined RainReflections
const int maxf = 3;				//number of refinements
const float ref = 0.11;			//refinement multiplier
const float inc = 3.0;			//increasement factor at each step

vec3 nvec3(vec4 pos) {
    return pos.xyz/pos.w;
}
vec4 nvec4(vec3 pos) {
    return vec4(pos.xyz, 1.0);
}
float cdist(vec2 coord) {
	return max(abs(coord.s-0.5),abs(coord.t-0.5))*2.0;
}

vec4 raytrace(vec3 fragpos, vec3 skycolor, vec3 rvector) {
    vec4 color = vec4(0.0);
    vec3 start = fragpos;
	rvector *= 1.2;
    fragpos += rvector;
	vec3 tvector = rvector;
    int sr = 0;

    for(int i=0;i<25;i++){
        vec3 pos = nvec3(gbufferProjection * nvec4(fragpos)) * 0.5 + 0.5;
        if(pos.x < 0 || pos.x > 1 || pos.y < 0 || pos.y > 1 || pos.z < 0 || pos.z > 1.0) break;
        vec3 fragpos0 = vec3(pos.st, texture2D(depthtex1, pos.st).r);
        fragpos0 = nvec3(gbufferProjectionInverse * nvec4(fragpos0 * 2.0 - 1.0));
        float err = distance(fragpos,fragpos0);
		if(err < pow(length(rvector),1.175)){ //if(err < pow(length(rvector)*1.85,1.15)){ <- old, adjusted error check to reduce banding issues/glitches
                sr++;
                if(sr >= maxf){
					bool land = texture2D(depthtex1, pos.st).r < comp;
                    color = pow(texture2DLod(gaux1, pos.st, 1),vec4(2.2))*257.0;
					if (isEyeInWater == 0) color.rgb = land ? mix(color.rgb,skycolor*(0.7+0.3*tmult)*(1.33-rainStrength*0.8),calcFog(fragpos0.xyz)) : drawSun(rvector,skycolor,1.0);
					color.a = clamp(1.0 - pow(cdist(pos.st), 20.0), 0.0, 1.0);
					break;
                }
				tvector -= rvector;
                rvector *= ref;

}
        rvector *= inc;
        tvector += rvector;
		fragpos = start + tvector;
    }
    return color;
}

vec4 raytraceLand(vec3 fragpos, vec3 skycolor, vec3 rvector) {
	const int maxf = 3;				//number of refinements
	const float ref = 0.11;			//refinement multiplier
	const float inc = 2.4;			//increasement factor at each step
	const float errorstep = 1.5;

    vec4 color = vec4(0.0);
    vec3 start = fragpos;
	rvector *= 1.2;
    fragpos += rvector;
	vec3 tvector = rvector;
    int sr = 0;

    for(int i=0;i<25;i++){
        vec3 pos = nvec3(gbufferProjection * nvec4(fragpos)) * 0.5 + 0.5;
        if(pos.x < 0 || pos.x > 1 || pos.y < 0 || pos.y > 1 || pos.z < 0 || pos.z > 1.0) break;
        vec3 fragpos0 = vec3(pos.st, texture2D(depthtex1, pos.st).r);
        fragpos0 = nvec3(gbufferProjectionInverse * nvec4(fragpos0 * 2.0 - 1.0));
        float err = distance(fragpos,fragpos0);
		if(err < pow(length(rvector),errorstep)){ //if(err < pow(length(rvector)*1.85,1.15)){ <- old, adjusted error check to reduce banding issues/glitches
                sr++;
                if(sr >= maxf){
					bool land = texture2D(depthtex1, pos.st).r < comp;
                    color = pow(texture2DLod(gaux1, pos.st, 1),vec4(2.2))*257.0;
					color.a = clamp(1.0 - pow(cdist(pos.st), 20.0), 0.0, 1.0);
					break;
                }
				tvector -= rvector;
                rvector *= ref;

}
        rvector *= inc;
        tvector += rvector;
		fragpos = start + tvector;
    }
    return color;
}
#endif

#if Clouds == 2 || Clouds == 4
float subSurfaceScattering(vec3 vec,vec3 pos, float N) {
	return pow(max(dot(vec,normalize(pos)),0.0),N)*(N+1)/6.28;
}

float noisetexture(vec2 coord){
	return texture2D(noisetex, coord).x;
}

vec3 drawCloud(vec3 fposition, vec3 color) {
const float r = 3.2;
const vec4 noiseC = vec4(1.0,r,r*r,r*r*r);
const vec4 noiseWeights = 1.0/noiseC/dot(1.0/noiseC,vec4(1.0));

vec3 tpos = vec3(gbufferModelViewInverse * vec4(fposition, 0.0));
tpos = normalize(tpos);

float cosT = max(dot(fposition, upVec),0.0);

float wind = abs(frameTimeCounter*0.0005-0.5)+0.5;
float distortion = wind * 0.045;
	
float iMult = -log(cosT)*2.0+2.0;
float heightA = (400.0+300.0*sqrt(cosT))/(tpos.y);

for (int i = 1;i<22;i++) {
	vec3 intersection = tpos*(heightA-4.0*i*iMult); 			//curved cloud plane
	vec2 coord1 = intersection.xz/200000.0+wind*0.05;
	vec2 coord = fract(coord1/0.25);
	
	vec4 noiseSample = vec4(noisetexture(coord+distortion),
							noisetexture(coord*noiseC.y+distortion),
							noisetexture(coord*noiseC.z+distortion),
							noisetexture(coord*noiseC.w+distortion));

	float j = i / 22.0;
	coord = vec2(j+0.5,-j+0.5)/noiseTextureResolution + coord.xy + sin(coord.xy*3.14*j)/10.0 + wind*0.02*(j+0.5);
	
	vec2 secondcoord = 1.0 - coord.yx;
	vec4 noiseSample2 = vec4(noisetexture(secondcoord),
							 noisetexture(secondcoord*noiseC.y),
							 noisetexture(secondcoord*noiseC.z),
							 noisetexture(secondcoord*noiseC.w));

	float finalnoise = dot(noiseSample*noiseSample2,noiseWeights);
	float cl = max((sqrt(finalnoise*max(1.0-abs(i-11.0)/11*(0.15-1.7*rainStrength),0.0))-0.55)/(0.65+2.0*rainStrength)*clamp(cosT*cosT*2.0,0.0,1.0),0.0);

	float cMult = max(pow(30.0-i,3.5)/pow(30.,3.5),0.0)*6.0;

	float sunscattering = subSurfaceScattering(sunVec, fposition, 75.0)*pow(cl, 3.75);
	float moonscattering = subSurfaceScattering(-sunVec, fposition, 75.0)*pow(cl, 5.0);
	
	color = color*(1.0-cl)+cl*cMult*mix(cloudColor2*4.75,cloudColor,min(cMult,0.875)) * 0.05 + sunscattering+moonscattering;
	}
return color;
}/*---------------------------*/
#endif

#if Clouds == 3 || Clouds == 4
float maxHeight = (cloud_height+200.0);

float densityAtPos(in vec3 pos){
	pos /= 18.0;
	pos.xz *= 0.5;

	vec3 p = floor(pos);
	vec3 f = fract(pos);
	
	f = (f*f) * (3.0-2.0*f);
	f = sqrt(f);
	vec2 uv = p.xz + f.xz + p.y * 17.0;

	vec2 coord =  uv / 64.0;
	vec2 coord2 =  uv / 64.0 + 17.0 / 64.0;
	float xy1 = texture2D(noisetex, coord).x;
	float xy2 = texture2D(noisetex, coord2).x;
	return mix(xy1, xy2, f.y);
}

float cloudPlane(in vec3 pos){
	float center = cloud_height*0.5+maxHeight*0.5;
	float difcenter = maxHeight-center;	
	float mult = (pos.y-center)/difcenter;
	
	vec3 samplePos = pos*vec3(0.5,0.5,0.5)*0.35+frameTimeCounter*vec3(0.5,0.,0.5);
	float noise = 0.0;
	float tot = 0.0;
	for(int i=0 ; i < 4; i++){
		noise += densityAtPos(samplePos*exp(i*1.05)*0.6+frameTimeCounter*i*vec3(0.5,0.,0.5)*0.6)*exp(-i*0.8);
		tot += exp(-i*0.8);
	}

return 1.0-pow(0.4,max(noise/tot-0.56-mult*mult*0.3+rainStrength*0.16,0.0)*2.2);
}

vec3 renderClouds(in vec3 pos, in vec3 color, const int cloudIT) {
	float dither = fract(0.75487765 * gl_FragCoord.x + 0.56984026 * gl_FragCoord.y);	
	#ifdef TAA	
		  dither = fract(frameTimeCounter * 256.0 + dither);
	#endif
		
	//setup
	vec3 dV_view = pos.xyz;
	vec3 progress_view = vec3(0.0);
	pos = pos*2200.0 + cameraPosition; //makes max cloud distance not dependant of render distance	
	
	//3 ray setup cases : below cloud plane, in cloud plane and above cloud plane
	if (cameraPosition.y <= cloud_height){
		float maxHeight2 = min(maxHeight, pos.y);	//stop ray when intersecting before cloud plane end
		
		//setup ray to start at the start of the cloud plane and end at the end of the cloud plane
		dV_view *= -(maxHeight2-cloud_height)/dV_view.y/cloudIT;
		progress_view = dV_view*dither + cameraPosition + dV_view*(maxHeight2-cameraPosition.y)/(dV_view.y);
		if (pos.y < cloud_height) return color;	//don't trace if no intersection is possible
	}
	if (cameraPosition.y > cloud_height && cameraPosition.y < maxHeight){
		if (dV_view.y <= 0.0) {	
		float maxHeight2 = max(cloud_height, pos.y);	//stop ray when intersecting before cloud plane end
		
		//setup ray to start at eye position and end at the end of the cloud plane
		dV_view *= abs(maxHeight2-cameraPosition.y)/abs(dV_view.y)/cloudIT;
		progress_view = dV_view*dither + cameraPosition + dV_view*cloudIT;
		dV_view *= -1.0;
		}
else if (dV_view.y > 0.0) {		
		float maxHeight2 = min(maxHeight, pos.y);	//stop ray when intersecting before cloud plane end
		
		//setup ray to start at eye position and end at the end of the cloud plane
		dV_view *= -abs(maxHeight2-cameraPosition.y)/abs(dV_view.y)/cloudIT;
		progress_view = dV_view*dither + cameraPosition - dV_view*cloudIT;
		}
	}
	if (cameraPosition.y >= maxHeight){			
		float maxHeight2 = max(cloud_height, pos.y);	//stop ray when intersecting before cloud plane end

		//setup ray to start at eye position and end at the end of the cloud plane
		dV_view *= -abs(maxHeight2-maxHeight)/abs(dV_view.y)/cloudIT;
		progress_view = dV_view*dither + cameraPosition + dV_view*(maxHeight2-cameraPosition.y)/dV_view.y;
		if (pos.y > maxHeight) return color;	//don't trace if no intersection is possible
	}

	float mult = length(dV_view)/256.0;

	for (int i=0;i<cloudIT;i++) {
		float cloud = cloudPlane(progress_view)*40.0;
		float lightsourceVis = pow(clamp(progress_view.y-cloud_height,0.,200.)/200.,2.3);
		color = mix(color,mix(cloudColor2*0.05, cloudColor*0.15, lightsourceVis),1.0-exp(-cloud*mult));

		progress_view += dV_view;
	}

	return color;	
}
#endif

#ifdef customStars
float calcStars(vec3 pos){
 	vec3 p = pos * 256.0;
	vec3 flr = floor(p);
	float fr = length((p - flr) - 0.5);
	flr = fract(flr * 443.8975);
    flr += dot(flr, flr.xyz + 19.19);

 	float intensity = step(fract((flr.x + flr.y) * flr.z), 0.0025) * (1.0 - rainStrength);
	float stars = clamp((fr - 0.5) / (0.0 - 0.5), 0.0, 1.0);	//recreate smoothstep for opengl 120
    	  stars = stars * stars * (3.0 - 2.0 * stars);			//^

 	return stars * intensity;
}
#endif

#ifdef Refraction
mat2 rmatrix(float rad){
	return mat2(vec2(cos(rad), -sin(rad)), vec2(sin(rad), cos(rad)));
}

float calcWaves(vec2 coord){
	vec2 movement = abs(vec2(0.0, -frameTimeCounter * 0.5));

	coord *= 0.4;
	vec2 coord0 = coord * rmatrix(1.0) - movement * 4.5;
		 coord0.y *= 3.0;
	vec2 coord1 = coord * rmatrix(0.5) - movement * 1.5;
		 coord1.y *= 3.0;		 
	vec2 coord2 = coord * frameTimeCounter * 0.02;
	
	coord0 *= (waveSize+0.5);	//create an offset for bigger waves
	coord1 *= waveSize;

	float wave = texture2D(noisetex,coord0 * 0.005).x * 10.0;			//big waves
		  wave -= texture2D(noisetex,coord1 * 0.010416).x * 7.0;			//small waves
		  wave += 1.0-sqrt(texture2D(noisetex,coord2 * 0.0416).x * 6.5) * 1.33;	//noise texture
		  wave *= 0.0157;

	return wave;
}

vec2 calcBump(vec2 coord){
	const vec2 deltaPos = vec2(0.25, 0.0);

	float h0 = calcWaves(coord);
	float h1 = calcWaves(coord + deltaPos.xy);
	float h2 = calcWaves(coord - deltaPos.xy);
	float h3 = calcWaves(coord + deltaPos.yx);
	float h4 = calcWaves(coord - deltaPos.yx);

	float xDelta = ((h1-h0)+(h0-h2));
	float yDelta = ((h3-h0)+(h0-h4));

	return vec2(xDelta,yDelta)*0.05;
}
#endif

uniform float wetness;
#ifdef RainReflections
const float wetnessHalflife = 200.0f;
const float drynessHalflife = 75.0f;

float noisetexture(vec2 coord, float offset, float speed){
	   speed *= (0.001+rainStrength); //generate static noise after raining stopped for an improved wetness effect
	   offset *= rainNoise;
return texture2D(noisetex, fract(coord*offset + frameTimeCounter*speed)).x/offset;
}

float calcRainripples(vec2 pos){
	float wave = noisetexture(pos, 1.75, 0.1125);
		  wave -= noisetexture(pos, 1.8, -0.1125);

	return wave;
}
#endif

vec3 decode (vec2 enc){
    vec2 fenc = enc*4-2;
    float f = dot(fenc,fenc);
    float g = sqrt(1-f/4.0);
    vec3 n;
    n.xy = fenc*g;
    n.z = 1-f/2;
    return n;
}

/*
vec2 texelSize = vec2(1.0/viewWidth,1.0/viewHeight);
uniform int framemod8;
const vec2[8] offsets = vec2[8](vec2(1./8.,-3./8.),
								vec2(-1.,3.)/8.,
								vec2(5.0,1.)/8.,
								vec2(-3,-5.)/8.,
								vec2(-5.,5.)/8.,
								vec2(-7.,-1.)/8.,
								vec2(3,7.)/8.,
								vec2(7.,-7.)/8.);

vec3 toScreenSpace(vec3 p) {
	vec4 iProjDiag = vec4(gbufferProjectionInverse[0].x, gbufferProjectionInverse[1].y, gbufferProjectionInverse[2].zw);
    vec3 p3 = p * 2.0 - 1.0;
    vec4 fragposition = iProjDiag * p3.xyzz + gbufferProjectionInverse[3];
    return fragposition.xyz / fragposition.w;
}

vec2 newTC = gl_FragCoord.xy*texelSize;
vec3 TAAfragpos = toScreenSpace(vec3(newTC-offsets[framemod8]*texelSize*0.5, texture2D(depthtex0, newTC).x));

#if defined waterRefl || defined iceRefl
vec3 toClipSpace(vec3 viewSpacePosition) {
	return (vec3(gbufferProjection[0].x, gbufferProjection[1].y, gbufferProjection[2].z) * viewSpacePosition + gbufferProjection[3].xyz) / -viewSpacePosition.z * 0.5 + 0.5;
}
float ld(float dist) {
    return (2.0 * near) / (far + near - dist * (far - near));
}

vec3 newTrace(vec3 dir, vec3 position, float dither, float fresnel){
	#define SSR_STEPS 40 //[10 15 20 25 30 35 40 50 100 200 400]
    float quality = mix(15.0,SSR_STEPS,fresnel);

    vec3 clipPosition = toClipSpace(position);
	float rayLength = ((position.z + dir.z * far*1.732) > -near) ? (-near -position.z) / dir.z : far*1.732;
    vec3 direction = normalize(toClipSpace(position+dir*rayLength)-clipPosition);  //convert to clip space

    //get at which length the ray intersects with the edge of the screen
    vec3 maxLengths = (step(0.0,direction)-clipPosition) / direction;
    float mult = min(min(maxLengths.x,maxLengths.y),maxLengths.z);
	
    vec3 stepv = direction * mult / quality;

	vec3 spos = clipPosition + stepv*dither;
	float minZ = clipPosition.z;
	float maxZ = spos.z+stepv.z*0.5;
	spos.xy+=offsets[framemod8]*texelSize*0.5;
	//raymarch on a quarter res depth buffer for improved cache coherency

    for (int i = 0; i < int(quality+1); i++) {
		//float sp = texelFetch2D(depthtex1, ivec2(spos.xy/texelSize), 0).x;
		float sp = texture2D(depthtex1, spos.xy).x;		//might be slower then texelfetch but doesn't require shader4 extension
        if(sp <= max(maxZ,minZ) && sp >= min(maxZ,minZ)) return vec3(spos.xy,sp);

        spos += stepv;
		//small bias
		minZ = maxZ-0.00004/ld(spos.z);
		maxZ += stepv.z;
    }

    return vec3(1.1);
}
#endif
*/
vec2 decodeVec2(float a){
    const vec2 constant1 = 65535. / vec2( 256., 65536.);
    const float constant2 = 256. / 255.;
    return fract( a * constant1 ) * constant2 ;
}

float ld(float depth) {
    return 1.0 / (far+near - depth * far-near);
}

void main() {

vec3 c = pow(texture2D(gaux1,texcoord).xyz,vec3(2.2))*257.;
vec3 hr = texture2D(composite,(floor(texcoord*vec2(viewWidth,viewHeight)/2.0)*2.0+1.0)/vec2(viewWidth,viewHeight)/2.0).rgb*30.0;
vec2 lightmap = texture2D(colortex1, texcoord.xy).zw;

//Depth and fragpos
float depth0 = texture2D(depthtex0, texcoord).x;
vec4 fragpos0 = gbufferProjectionInverse * (vec4(texcoord, depth0, 1.0) * 2.0 - 1.0);
fragpos0 /= fragpos0.w;
vec3 normalfragpos0 = normalize(fragpos0.xyz);

float depth1 = texture2D(depthtex1, texcoord).x;
vec4 fragpos1 = gbufferProjectionInverse * (vec4(texcoord, depth1, 1.0) * 2.0 - 1.0);
	 fragpos1 /= fragpos1.w;
vec3 normalfragpos1 = normalize(fragpos1.xyz);
/*--------------------------------------------------------------------------------------------*/
float mats = texture2D(colortex0,texcoord).b;

vec4 trp = texture2D(gaux3,texcoord.xy);

bool transparency = dot(trp.xyz,trp.xyz) > 0.000001;
bool isMetallic = mats > 0.39 && mats < 0.41;
bool isPolished = mats > 0.49 && mats < 0.51;
#ifndef polishedRefl
	isPolished = false;
#endif	
#ifndef metallicRefl
	isMetallic = false;
#endif	

#ifdef Refraction
if (texture2D(colortex2, texcoord).z < 0.2499 && dot(texture2D(colortex2,texcoord).xyz,texture2D(colortex2,texcoord).xyz) > 0.0 || isEyeInWater == 1.0) { //reflective water
	vec2 wpos = (gbufferModelViewInverse*fragpos0).xz+cameraPosition.xz;
	vec2 refraction = texcoord.xy + calcBump(wpos);
	float caustics = 0.0;

	#ifdef Caustics
	float skylight = pow(max(lightmap.y-1.0/16.0,0.0)*1.14285714286, 0.5);
	//vec2 wposC = (gbufferModelViewInverse*fragpos1).xz+cameraPosition.xz;
	caustics = calcWaves(wpos*3.0);
	caustics = mix(caustics*4.0, 1.0, 1.0-skylight);
	caustics = clamp(caustics*causticsStrength, -1.0, 0.1); //adjust color and filter out dark parts
	caustics *= (1.0-rainStrength);
	#endif

	c = pow(texture2D(gaux1, refraction).xyz, vec3(2.2) + caustics)*257.0;
	#ifdef uwatertint
	if(isEyeInWater == 1.0){
		vec3 underwaterC = vec3(0.0,0.0025,0.0075) * (1.0-0.95*night) * (1.0-0.95*rainStrength);	 //tint underwater
		c = mix(c, underwaterC, 0.75);
	}
	#endif

}
#endif

bool island = !(dot(texture2D(colortex0,texcoord).rgb,vec3(1.0))<0.00000000001 || (depth1 > comp)); //must be depth1 > comp for transparency

if (!island){ //breaks transparency rendering against the sky
vec3 cpos = normalize(gbufferModelViewInverse*fragpos1).xyz;
#ifdef defskybox
	c = mix(c.rgb, hr.rgb, skyboxblendfactor);	//blend skytexture with shader skycolor
#else
	c = hr.rgb;
#endif
#ifdef customStars
	c += calcStars(cpos)*moonVisibility;
#endif
#if Clouds == 2 || Clouds == 4
	c = drawCloud(normalfragpos1.xyz, c);
#endif
#ifndef defskybox
	c = drawSun(fragpos1.xyz, c, 1.0);
	c = drawMoon(fragpos1.xyz, c, 1.0);
#endif
#if Clouds == 3 || Clouds == 4
float cheight = (cloud_height-32.0);
if (dot(fragpos1.xyz, upVec) > 0.0 || cameraPosition.y > cheight)c = renderClouds(cpos, c, cloudsIT);
#endif
}/*--------------------------------------*/

	//Draw fog
	vec3 fogC = hr.rgb*(0.7+0.3*tmult)*(1.33-rainStrength*0.67);
	float fogF = calcFog(fragpos0.xyz);
	/*----------------------------------------------------------------*/

//Render before transparency
#if defined metallicRefl || defined polishedRefl
	if (isMetallic || isPolished) {
		vec3 relfNormal = decode(texture2D(colortex1,texcoord).xy);
		vec3 reflectedVector = reflect(normalfragpos0, relfNormal);
		
		float normalDotEye = dot(relfNormal, normalfragpos0);
		float fresnel = pow(clamp(1.0 + normalDotEye,0.0,1.0), 4.0);
			  fresnel = mix(0.09,1.0,fresnel); //F0
	
		vec3 sky_c = getSkyc(reflectedVector)*metallicSky;
		vec4 reflection = raytrace(fragpos0.xyz, sky_c, reflectedVector);

		reflection.rgb = mix(sky_c, reflection.rgb, reflection.a)*0.5;
		c = mix(c,reflection.rgb,fresnel*metalStrength);
	}
#endif

	//Draw entities etc
	//c = c*(1.0-texture2D(colortex2, texcoord).a)+texture2D(colortex2, texcoord).rgb;

if (transparency) {
	vec3 normal = texture2D(colortex2,texcoord).xyz;
	float sky = normal.z;

	bool reflectiveWater = sky < 0.2499 && dot(normal,normal) > 0.0;
	bool reflectiveIce = sky > 0.2499 && sky < 0.4999 && dot(normal,normal) > 0.0;

	bool iswater = sky < 0.2499;
	bool isice = sky > 0.2499 && sky < 0.4999;

	if (iswater) sky *= 4.0;
	if (isice) sky = (sky - 0.25)*4.0;
	if (!iswater && !isice) sky = (sky - 0.5)*4.0;

	sky = clamp(sky*1.2-2./16.0*1.2,0.,1.0);
	sky *= sky;

	normal = normalize(decode(normal.xy));
	
	//draw fog for transparency
	float iswater2 = float(iswater);
	float skylight = pow(max(lightmap.y-2.0/16.0,0.0)*1.14285714286, 1.0);
	c = mix(c, fogC, fogF-fogF) / (1.0 + 5.0*night*iswater2*skylight);

	//Draw transparency
	vec3 finalAc = texture2D(gaux2, texcoord.xy).rgb;
	float alphaT = clamp(length(trp.rgb)*1.02,0.0,1.0);

	c = mix(c,c*(trp.rgb*0.9999+0.0001)*1.732,alphaT)*(1.0-alphaT) + finalAc;
	/*-----------------------------------------------------------------------------------------*/
/*
	#if defined waterRefl || defined iceRefl
	if (reflectiveWater || reflectiveIce) {
		vec3 wFragpos1 = normalize(TAAfragpos);	
		vec3 reflectedVector = reflect(wFragpos1, normal);

		float normalDotEye = dot(normal, wFragpos1);
		float fresnel = pow(clamp(1.0 + normalDotEye,0.0,1.0), 4.0);
			  fresnel = mix(0.09,1.0,fresnel); //F0
			  fresnel *= 0.75;

		vec3 sky_c = getSkyc(reflectedVector);
			 sky_c = (isEyeInWater == 0)? ((drawSun(reflectedVector, sky_c, 1.0)+drawMoon(reflectedVector, sky_c, 1.0)) * 0.5)*sky : pow(vec3(0.25,0.5,0.72),vec3(2.2))*rawAvg*0.1;		

	#if defined Cloud_reflection && (Clouds == 2 || Clouds == 3 || Clouds == 4)
		vec3 cloudVector = normalize(gbufferModelViewInverse*vec4(reflect(fragpos1.xyz, normal), 1.0)).xyz;
		#if Clouds == 2 || Clouds == 4
			sky_c += drawCloud(reflectedVector, vec3(0.0))*1.5;
		#endif
		#if Clouds == 3 || Clouds == 4
			sky_c += renderClouds(cloudVector, c, cloudreflIT); 
		#endif
	#endif

		vec4 reflColor = vec4(sky_c, 0.0);
		vec3 reflection = newTrace(reflectedVector, fragpos0.xyz, fract(dot(gl_FragCoord.xy, vec2(0.5, 0.25))), fresnel);

		if (reflection.z < 1.0){	
			reflColor.rgb = pow(texture2D(gaux1, reflection.xy).rgb, vec3(2.2))*257.0;
			reflColor.a = 1.0;

			bool land = texture2D(depthtex1, reflection.xy).x < comp;
			if (isEyeInWater == 0) reflColor.rgb = land ? mix(reflColor.rgb, sky_c.rgb*(0.7+0.3*tmult)*(1.33-rainStrength*0.8), calcFog(fragpos0.xyz)) : drawSun(reflectedVector.rgb, sky_c, 1.0);
		}
		#ifndef waterRefl
			if(reflectiveWater)reflColor.a = 0.0;
		#endif
		#ifndef iceRefl
			if(reflectiveIce)reflColor.a = 0.0;
		#endif
		reflColor.rgb = mix(sky_c.rgb, reflColor.rgb, reflColor.a);
		c.rgb = mix(c.rgb, reflColor.rgb, fresnel);
	}
	#endif
*/


	if (reflectiveWater || reflectiveIce) {
		vec3 reflectedVector = reflect(normalfragpos0, normal);
		
		float normalDotEye = dot(normal, normalfragpos0);
		float fresnel = pow(clamp(1.0 + normalDotEye,0.0,1.0), 4.0);
			  fresnel = mix(0.09,1.0,fresnel); //F0
	
		vec3 sky_c = getSkyc(reflectedVector);

	#if defined Cloud_reflection && (Clouds == 2 || Clouds == 3 || Clouds == 4)
		vec3 cloudVector = normalize(gbufferModelViewInverse*vec4(reflect(fragpos0.xyz, normal), 1.0)).xyz;
		#if Clouds == 2 || Clouds == 4
			sky_c += drawCloud(reflectedVector, vec3(0.0))*1.5;
		#endif
		#if Clouds == 3 || Clouds == 4
			sky_c += renderClouds(cloudVector, c, cloudreflIT); 
		#endif
	#endif
	#if defined waterRefl || defined iceRefl
		vec4 reflection = raytrace(fragpos0.xyz, sky_c, reflectedVector);
	#else
		vec4 reflection = vec4(0.0);
	#endif	
	#ifndef waterRefl
		if(reflectiveWater){ reflection = vec4(0.0); fresnel *= 0.5; }
	#endif
	#ifndef iceRefl
		if(reflectiveIce){ reflection = vec4(0.0); fresnel *= 0.5; }
	#endif	

		sky_c = (isEyeInWater == 0)? ((drawSun(reflectedVector, sky_c, 1.0)+drawMoon(reflectedVector, sky_c, 1.0)) * 0.5)*sky : pow(vec3(0.25,0.5,0.72),vec3(2.2))*rawAvg*0.1;
		reflection.rgb = mix(sky_c, reflection.rgb, reflection.a)*0.85;

		c = mix(c,reflection.rgb,fresnel);
	}


}
	bool land = depth0 < comp;

#ifdef RainReflections
	vec3 wnormal = decode(texture2D(colortex1,texcoord).xy);

	float sky_lightmap = max(lightmap.y-2.0/16.0,0.0)*1.14285714286;
	float iswet = wetness*pow(sky_lightmap, 20.0)*sqrt(max(dot(wnormal,upVec),0.0));

	#ifdef BiomeCheck
		bool isRaining = (BiomeTemp >= 0.15) && (BiomeTemp <= 1.0) && iswet > 0.01 && land && !transparency && isEyeInWater == 0.0;
	#else
		bool isRaining = iswet > 0.01 && land && !transparency && isEyeInWater == 0.0;
	#endif

	if (isRaining) {
		vec2 wpos = (gbufferModelViewInverse*fragpos0).xz+cameraPosition.xz;

		wnormal *= 1.0+calcRainripples(wpos); //modify normals with noise / rain ripples before reflecting
		vec3 reflectedVector = reflect(fragpos0.xyz, wnormal); //TODO Improve reflections distortion

		float normalDotEye = dot(normalize(reflectedVector - fragpos0.xyz), normalfragpos0.xyz);
		float fresnel = pow(clamp(1.0 + normalDotEye,0.0,1.0), 4.0);
		fresnel = fresnel+0.09*(1.0-fresnel);
	
		vec3 sky_c = getSkyc(reflectedVector);
		vec4 reflection = raytraceLand(fragpos0.xyz, sky_c, normalize(reflectedVector));
			 reflection.rgb = mix(sky_c, reflection.rgb, reflection.a)*0.5;
	
		c = mix(c.rgb, reflection.rgb, fresnel*iswet); //smooth out wetness start and end
		//c.rgb = wnormal;
	}
	#endif

	//Draw land and underwater fog
	vec3 foglandC = fogC;
		 foglandC.b *= (1.5-0.5*rainStrength);
	if(land)c = mix(c,foglandC*(1.0-isEyeInWater),fogF);
#if Clouds == 3
	if(!land && rainStrength > 0.01)c = mix(c,fogC,fogF*0.75);	//tint vl clouds with fog while raining
#endif

	vec3 ufogC = sky1.rgb*0.1;
		// ufogC.g *= 1.0+2.0*inSwamp; //make fog greenish in swamp biomes
		 ufogC.r *= 0.0;
		 ufogC *= (1.0-0.85*night);
	
	#ifdef Underwater_Fog
	if (isEyeInWater == 1.0) c = mix(c, ufogC, 1.0-exp(-length(fragpos0.xyz)/uFogDensity));
	#endif
	
	if (isEyeInWater == 2.0) c = mix(c, vec3(1.0, 0.0125, 0.0), 1.0-exp(-length(fragpos0.xyz))); //lava fog
	if(blindness > 0.9) c = mix(c, vec3(0.0), 1.0-exp(-length(fragpos1.xyz)*1.125));	//blindness fog


//Draw rain
vec4 rain = texture2D(gaux4, texcoord);
if (rain.r > 0.0001 && rainStrength > 0.01 && !(depth1 < texture2D(depthtex2, texcoord).x)){
	float rainRGB = 0.25;
	float rainA = rain.r;

	float torch_lightmap = 12.0 - min(rain.g/rain.r * 12.0,11.0);
	torch_lightmap = 0.05 / torch_lightmap / torch_lightmap - 0.002595;

	vec3 rainC = rainRGB*(pow(max(dot(normalfragpos0, sunVec)*0.1+0.9,0.0),6.0)*(0.1+tr*0.9)*pow(sunlight,vec3(0.25))*sunVisibility+pow(max(dot(normalfragpos0, -sunVec)*0.05+0.95,0.0),6.0)*48.0*moonlight*moonVisibility)*0.04 + 0.05*rainRGB*length(avgAmbient2);
	rainC += torch_lightmap*vec3(emissive_R,emissive_G,emissive_B);
	c = c*(1.0-rainA*0.3)+rainC*1.5*rainA;
}


#ifndef Volumetric_Lighting
#ifdef Godrays
	float sunpos = abs(dot(normalfragpos0,normalize(sunPosition.xyz)));
	float illuminationDecay = pow(sunpos,30.0)+pow(sunpos,16.0)*0.8+pow(sunpos,2.0)*0.125;
	
	vec2 deltaTextCoord = (lightPos-texcoord)*0.01;
	vec2 textCoord = texcoord*0.5+0.5;

	float gr = texture2DLod(gaux1, textCoord + deltaTextCoord,1).a;
		  gr += texture2DLod(gaux1, textCoord + 2.0 * deltaTextCoord,1).a;
		  gr += texture2DLod(gaux1, textCoord + 3.0 * deltaTextCoord,1).a;
		  gr += texture2DLod(gaux1, textCoord + 4.0 * deltaTextCoord,1).a;
		  gr += texture2DLod(gaux1, textCoord + 5.0 * deltaTextCoord,1).a;
		  gr += texture2DLod(gaux1, textCoord + 6.0 * deltaTextCoord,1).a;
		  gr += texture2DLod(gaux1, textCoord + 7.0 * deltaTextCoord,1).a;

	vec3 finalGC = lightColor;
		if(isEyeInWater == 1.0) finalGC.rgb = ufogC*2.0; //Underwater godrays should use underwater fog color
	vec3 grC = finalGC*Godrays_Density;
	if(blindness < 1.0)c += grC*gr/7.0*illuminationDecay;
#endif
#endif

#ifdef Volumetric_Lighting
const float exposure = 1.05;

//sun-moon switch
vec3 lightVec = -sunVec;
vec3 lightcol = moonlight*5.0;
if (sunVisibility > 0.2){
	lightVec = sunVec;
	lightcol = sunlight;
}

float phase = 2.5+exp(dot(normalfragpos0,lightVec)*3.0)/3.0;
float vgr = texture2DLod(gaux1, texcoord, 1).a;

vec3 vgrC = lightcol*exposure*phase*0.08*(0.25+0.75*tmult*tmult)*tr*(1.0+pow(1.0-eyeBrightnessSmooth.y/255.0,2.0))*(1.0-rainStrength*0.9);
if (depth0 > comp)vgrC = mix(vgrC, c, 0.5);
c += vgrC*vgr*(1.0-isEyeInWater)*(float(land)*0.2+0.8);
#endif

#ifdef Lens_Flares
c += texture2D(composite,texcoord.xy*0.5+0.5+1.0/vec2(viewWidth,viewHeight)).rgb*fading*30*30/100*pow(dot(texture2D(gaux1, vec2(1.0)/vec2(viewWidth,viewHeight)).w, 1.0), 2.0);
#endif

	c = (c/50.0*pow(eyeAdapt,0.88));
	gl_FragData[0] = vec4(c,1.0);
}

