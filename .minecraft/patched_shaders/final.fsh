#version 150 core
#define gl_FragData iris_FragData
#define gl_FragColor iris_FragData[0]
#define varying in
#define gl_ModelViewProjectionMatrix (gl_ProjectionMatrix * gl_ModelViewMatrix)
#define gl_ModelViewMatrix mat4(1.0)
#define gl_NormalMatrix mat3(1.0)
#define gl_Color vec4(1.0, 1.0, 1.0, 1.0)
#define gl_ProjectionMatrix mat4(1.0)
#define gl_FogFragCoord iris_FogFragCoord
uniform float iris_FogDensity;
uniform float iris_FogStart;
uniform float iris_FogEnd;
uniform vec4 iris_FogColor;

struct iris_FogParameters {
    vec4 color;
    float density;
    float start;
    float end;
    float scale;
};

iris_FogParameters iris_Fog = iris_FogParameters(iris_FogColor, iris_FogDensity, iris_FogStart, iris_FogEnd, 1.0 / (iris_FogEnd - iris_FogStart));

#define gl_Fog iris_Fog
in float iris_FogFragCoord;
out vec4 iris_FragData[8];
vec4 texture2D(sampler2D sampler, vec2 coord) { return texture(sampler, coord); }
vec4 texture2D(sampler2D sampler, vec2 coord, float bias) { return texture(sampler, coord, bias); }
vec4 texture2DLod(sampler2D sampler, vec2 coord, float lod) { return textureLod(sampler, coord, lod); }
vec4 shadow2D(sampler2DShadow sampler, vec3 coord) { return vec4(texture(sampler, coord)); }
vec4 shadow2DLod(sampler2DShadow sampler, vec3 coord, float lod) { return vec4(textureLod(sampler, coord, lod)); }

#define final
/*
Thank you for downloading Sildur's vibrant shaders, make sure you got it from the official source found here:
https://sildurs-shaders.github.io/
*/
#ifdef gbuffers_shadows
    #define Shadows								//Toggle all shadows
    #define SHADOW_MAP_BIAS 0.80
    #define grass_shadows                       //Also disables tallgrass and flowers shadows
    #ifdef Shadows
    const float shadowDistance = 70.0;			//Render distance of shadows. 60=lite, 80=med, 80=high, 120=extreme [60.0 70.0 80.0 90.0 100.0 110.0 120.0 130.0 140.0 150.0 160.0 170.0 180.0 190.0 200.0 210.0 220.0 230.0 240.0 250.0 260.0 270.0 280.0 290.0 300.0 310.0 320.0 330.0 340.0 350.0 360.0 370.0 380.0 390.0 400.0]
    const int shadowMapResolution = 512;		//Shadows resolution. [256 512 1024 2048 3072 4096 6144 8192 16384] 512=lite, 1024=med, 2048=high, 3072=extreme 
    const float k = 1.8;
    #define Nearshadowplane 0.05	            //[0.04 0.045 0.05 0.055 0.06 0.065 0.07 0.075 0.08 0.085 0.09 0.095 0.1] close quality, lower=higher quality, 0.09 is required for 2.0 farshadowmap to prevent glitches
    #define Farshadowplane 1.4                  //[0.6 0.8 1.0 1.2 1.4 1.6 1.8 2.0] far quality, streches the shadowmap, lower values closer, higher further away: 0.8=extreme, 1.0=high, 1.2=medium, 1.4=lite
    float a = exp(Nearshadowplane);
    float b = (exp(Farshadowplane)-a)*shadowDistance/128.0;
    float calcDistortion(vec2 worldpos){
        return 1.0/(log(length(worldpos)*b+a)*k);
    }
    #endif
#endif

#ifdef composite0
    #define ColoredShadows						//Toggle colored shadows
    //#define Penumbra                            //Toggle penumbra soft shadows
    //#define raytracedShadows                  //Improves closeup and faraway shadows. Also allows shadows to be cast outside of the shadowmap, outside of your shadows render distance. Requires shadows to be enabled. Has some issues since it's raytraced in screenspace.
    const int VPS_samples = 8;                  //Used for penumbra shadows [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30]
    #define shadow_samples 10                   //Used for shadows in general [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30]
    const bool 	shadowHardwareFiltering0 = true;
    const bool 	shadowHardwareFiltering1 = true;
    const float	sunPathRotation	= -40.0;		//[-10.0 -20.0 -30.0 -40.0 -50.0 -60.0 -70.0 -80.0 0.0 10.0 20.0 30.0 40.0 50.0 60.0 70.0 80.0]
    
    //#define SSDO				                //Ambient Occlusion, makes lighting more realistic. High performance impact.
        #define ao_strength 3.0                 //[1.0 2.0 3.0 4.0 5.0 6.0 7.0 8.0 9.0 10.0]

    #define Godrays
        const int grays_sample = 17;            //17=lite, 17=med, 20=high, 23=extreme
    //#define Volumetric_Lighting               //Disable godrays before enabling volumetric lighting.

    //#define Lens_Flares

    //#define Celshading                        //Cel shades everything, making it look somewhat like Borderlands. Zero performance impact.
        #define Celborder 1.0                   //[1.0 1.2 1.4 1.6 1.8 2.0 2.2 2.4 2.6 2.8 3.0]
	    #define Celradius 1.0                   //[1.0 0.9 0.8 0.7 0.6 0.5 0.4 0.3 0.2 0.1 0.075 0.05]

    //#define Whiteworld                        //Makes the ground white, screenshot -> https://i.imgur.com/xziUB8O.png

    #define Moonlight 0.003                     //[0.0 0.0015 0.003 0.006 0.009]

    //#define defskybox

    #define emissive_R 1.5                      //[0.0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0]
    #define emissive_G 0.42                     //[0.0 0.1 0.2 0.3 0.42 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0]
    #define emissive_B 0.045                    //[0.0 0.045 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0]
    #define minlight 0.002                      //Tweak the amount of minimal light inside caves etc. [0.001 0.002 0.003 0.004 0.005 0.006 0.007 0.008 0.009 0.010 0.011 0.012 0.013 0.014 0.015 0.016 0.017 0.018 0.019 0.020 0.021 0.022 0.023 0.024 0.025 0.026 0.027 0.028 0.029 0.030]
    
    #ifdef Penumbra
        //required for optifine to parse it since it doesn't parse #if defined penumbra, nvidia, windows.
    #endif

    //Use the same color as water for water shading, diffuse
    #define waterCR 0.0	        //[0.0 0.1 0.125 0.15 0.175 0.2 0.225 0.25 0.275 0.3 0.325 0.35 0.375 0.4 0.6 0.8 1.0 1.2 1.4 1.6 1.8 2.0 2.2 2.4 2.6 2.8 3.0]
    #define waterCG 0.175	    //[0.0 0.1 0.125 0.15 0.175 0.2 0.225 0.25 0.275 0.3 0.325 0.35 0.375 0.4 0.6 0.65 0.8 1.0 1.2 1.4 1.6 1.8 2.0 2.2 2.4 2.6 2.8 3.0]
    #define waterCB 0.2	        //[0.0 0.1 0.125 0.15 0.175 0.2 0.225 0.25 0.275 0.3 0.325 0.35 0.375 0.4 0.6 0.8 1.0 1.2 1.25 1.4 1.6 1.8 2.0 2.2 2.4 2.6 2.8 3.0]    
#endif

#ifdef composite1
	const int noiseTextureResolution = 128;     //must be in composite1 for 2d clouds
    
	#define Godrays
		#define Godrays_Density 1.15            //[0.575 1.15 2.3 4.6 9.2]
	//#define Lens_Flares

	//#define Volumetric_Lighting               //Disable godrays before enabling volumetric lighting.
	
	#define Fog                                 //Toggle fog
		#define wFogDensity	100.0               //adjust fog density [5.0 10.0 15.0 20.0 25.0 30.0 35.0 40.0 45.0 50.0 55.0 60.0 65.0 70.0 75.0 80.0 90.0 100.0 110.0 120.0 130.0 140.0 150.0 160.0 170.0 180.0 190.0 200.0 210.0 220.0 230.0 240.0 250.0 260.0 270.0 280.0 290.0 300.0]		
        //#define morningFog                    //Toggle dynamic fog during sunrise.
    #define Underwater_Fog                      //Toggle underwater fog. 
		#define uFogDensity 25.0                //adjust underwater fog density [5.0 10.0 15.0 20.0 25.0 30.0 35.0 40.0 45.0 50.0 55.0 60.0 65.0 70.0 75.0 80.0 90.0 100.0 110.0 120.0 130.0 140.0 150.0 160.0 170.0 180.0 190.0 200.0]		
		#define uwatertint                      //Tints the underwater ground with blue color.

	#define Clouds 3                            //[0 1 2 3 4] Toggle clouds. 0=Off, 1=Default MC, 2=2D, 3=VL, 4=2D+VL, also adjust in gbuffers_cloud
		#define cloudsIT 6                      //[6 8 10 12 14 16 18 20 24 32 48] Volumetric clouds quality.
		#define cloudreflIT 2                   //[2 4 6 8 10 12 14 16] Reflected volumetric clouds quality.
		//#define Cloud_reflection              //Toggle clouds reflection in water	
        #define cloud_height 256.0		        //[64.0 80.0 96.0 112.0 128.0 144.0 160.0 176.0 192.0 208.0 224.0 240.0 256.0 272.0 288.0 304.0 320.0 336.0 352.0 368.0 384.0 400.0]

	//#define waterRefl                           //Toggle water reflections
	//#define iceRefl                             //Toggle stained glass and ice reflections
	
    //#define Refraction                          //Toggle water refractions.
    //#define Caustics                            //Toggle water caustics.
    #define causticsStrength 0.8                //[0.1 0.15 0.2 0.25 0.30 0.35 0.4 0.45 0.5 0.55 0.6 0.65 0.7 0.75 0.8 0.85 0.9 0.95 1.0 1.05 1.10 1.15 1.20 1.25 1.3 1.35 1.4 1.45 1.5 1.55 1.6 1.65 1.75 1.8 1.85 1.9 1.95 2.0]
    #define waveSize 1.0                        //[0.25 0.5 0.75 1.0 1.25 1.5 1.75 2.0 2.25 2.5 2.75 3.0] Adjust water wave size, includes refraction and caustic size.

    //#define metallicRefl                        //Toggle reflections for metallic blocks defined in block.properties
    //#define polishedRefl                        //Toggle reflections for polished blocks defined in block.properties   
    #define metalStrength 1.0                   //[0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0] Metallic and polished reflection strength
    #define metallicSky 0.1                     //[0.0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0] Sky reflection strength

	//#define RainReflections                   //Toggle rain reflections, wetness
	#define BiomeCheck                          //Toggle biome check for rain reflections. Only enable if no reflections are present. Updating optifine might resolve missing reflections with this on.
	#define rainNoise 1.0                       //[1.0 2.0 3.0 4.0 5.0 6.0 7.0 8.0 9.0 10.0] Higher values means less noise

	#define customStars                         //Toggle custom stars.	

    //#define defskybox                         //Toggle support for the default skybox, including custom skies from resourcepacks. If no resourcepack is present this will only enable the default sun and moon texture. This option also auto disables the custom shader sun and moon.
    #define skyboxblendfactor 0.75              //[0.0 0.05 0.1 0.15 0.2 0.25 0.3 0.35 0.4 0.45 0.5 0.55 0.6 0.65 0.7 0.75 0.8 0.85 0.9 0.95 1.0] Adjust the blend factor of custom sky and default skybox

	#define emissive_R 1.5                      //[0.0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0]
	#define emissive_G 0.42                     //[0.0 0.1 0.2 0.3 0.42 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0]
	#define emissive_B 0.045                    //[0.0 0.045 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0]

    #ifdef Cloud_reflection
        //required for optifine to parse it since it doesn't parse #if defined
    #endif  
    #ifdef waterRefl
        //required for optifine to parse it since it doesn't parse #if defined
    #endif    
#endif

#ifdef composite2
    //#define TAA							        //Toggle temporal anti-aliasing (TAA)
    #define TAA_quality	2				        //[1 2] Fast is more blurry during movement compared to fancy.

    //#define Adaptive_sharpening			    //Toggle adaptive sharpening. Recommended to use with TAA. Disabling TAA also disables adaptive sharpening.
    #define AS_sharpening 0.5 			        //[0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0] Adjust sharpening strength.
#endif

#ifdef composite3
    //#define Bloom
#endif    

#ifdef composite4
    //#define Bloom				                //Also disables sun glare
    #define bloom_strength 0.75	                //Adjust bloom strength [0.5 0.75 1.0 2.0]
#endif

#ifdef final
    //#define Bloom

    #define Rain_Drops			                //Enables rain drops on screen during raining. Requires sun effects to be enabled. Low performance impact.

    //#define Refraction

    //#define Depth_of_Field	                //Simulates eye focusing on objects. Low performance impact
	    //#define Distance_Blur	                //Requires Depth of Field to be enabled. Replaces eye focusing effect with distance being blurred instead.
        #define smoothDof                       //Toggle smooth transition between clear and blurry.

    //#define Motionblur		                //Blurres your view/camera during movemenent. Low performance impact. Doesn't work with Depth of Field.

    #define Cloudsblur			                //Blurres the sky abit, making volumetric clouds less noisy. Doesn't work with MB and DoF
	
    //Defined values for Optifine
    #define DoF_Strength 90		                //[10 20 30 40 50 60 70 80 90 100 110 120 130 140 150 160 170 180 190 200 210 220 230 240 250 260 270 280 290 300]
    #define Dof_Distance_View 256               //[128 256 384 512]
    #define MB_strength 0.014	                //[0.008 0.014 0.020]
    #define Contrast 2.2                        //Lower values increase contrast while higher values reduce conrast. If you adjust contrast make sure adjust brightness aswell. [1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0 2.1 2.2 2.3 2.4 2.5 2.6 2.7 2.8 2.9 3.0 3.1 3.2 3.3 3.4 3.5 3.6 3.7 3.8 3.9 4.0 4.1 4.2 4.3 4.4 4.5 4.6 4.7 4.8 4.9 5.0] 
	#define Brightness 1.0                      //Adjust brightness. [0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0 2.1 2.2 2.3 2.4 2.5 2.6 2.7 2.8 2.9 3.0 3.1 3.2 3.3 3.4 3.5 3.6 3.7 3.8 3.9 4.0 4.1 4.2 4.3 4.4 4.5 4.6 4.7 4.8 4.9 5.0]

    #define Showbuffer 0	                    //[0 1 2 25 3 35 4 5 6 7 8]
#endif
 
#ifdef gbuffers_skytextured
    //#define defskybox
#endif

#ifdef gbuffers_terrain
    #define nMap 0				//[0 1 2]0=Off 1=Bumpmapping, 2=Parallax
    #define POM_RES 32			//Texture / Resourcepack resolution. [32 64 128 256 512 1024 2048]
    #define POM_DIST 16.0		//[8.0 16.0 24.0 32.0 40.0 48.0 56.0 64.0 72.0 80.0 88.0 96.0 104.0 112.0 120.0 128.0]
    #define POM_DEPTH 0.30		//[0.05 0.10 0.15 0.20 0.25 0.30 0.35 0.40 0.45 0.50 0.55 0.60 0.65 0.70 0.75 0.80 0.85 0.90 0.95 1.0]

    //#define metallicRefl        //Toggle reflections for blocks defined in block.properties
    //#define polishedRefl        //Toggle reflections for polished blocks defined in block.properties

    #define Waving_Leaves
    #define Waving_Vines
    #define Waving_Grass		//Does not include tallgrass due some issues with it.
    #define Waving_Tallgrass
    #define Waving_Fire
    #define Waving_Lava
    #define Waving_Lilypads
    #define Waving_Lanterns   
    #define Waving_Entities		//Includes: Saplings, small flowers, wheat, carrots, potatoes and beetroot.
    #define animationSpeed 1.0  //[0.25 0.5 0.75 1.0 1.25 1.5 1.75 2.0]
#endif

#ifdef gbuffers_texturedblock   //textured and block are mostly the same so lets use the same defines
    #define MobsFlashRed
    #define emissive_R 1.5		//[0.0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0]
    #define emissive_G 0.42		//[0.0 0.1 0.2 0.3 0.42 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0]
    #define emissive_B 0.045	//[0.0 0.045 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0]
    #define minlight 0.002      //Tweak the amount of minimal light inside caves etc. [0.001 0.002 0.003 0.004 0.005 0.006 0.007 0.008 0.009 0.010 0.011 0.012 0.013 0.014 0.015 0.016 0.017 0.018 0.019 0.020 0.021 0.022 0.023 0.024 0.025 0.026 0.027 0.028 0.029 0.030]
#endif

#ifdef gbuffers_water
    #define Waving_Water
    #define waves_amplitude 0.65    //[0.55 0.65 0.75 0.85 0.95 1.05 1.15 1.25 1.35 1.45 1.55 1.65 1.75 1.85 1.95 2.05]
    #define waveSize 1.0            //[0.25 0.5 0.75 1.0 1.25 1.5 1.75 2.0 2.25 2.5 2.75 3.0] Adjust water wave size, includes refraction and caustic size.

    //#define watertex
    #define wtexblend 0.12      //[0.02 0.03 0.04 0.05 0.06 0.07 0.08 0.09 0.10 0.11 0.12 0.13 0.14 0.15 0.16 0.17 0.18 0.19 0.20 0.21 0.22 0.23 0.24 0.25 0.26 0.27 0.28 0.29 0.30 0.31 0.32 0.33 0.34 0.35 0.36 0.37 0.38 0.39 0.40 0.41 0.42 0.43 0.44 0.45 0.46 0.47 0.48 0.49 0.50]
    //#define WaterParallax
    #define waterheight 1.0     //[1.0 1.5 2.0] height for parallax mapping

    #define waterCR 0.0	        //[0.0 0.1 0.125 0.15 0.175 0.2 0.225 0.25 0.275 0.3 0.325 0.35 0.375 0.4 0.6 0.8 1.0 1.2 1.4 1.6 1.8 2.0 2.2 2.4 2.6 2.8 3.0]
    #define waterCG 0.175	    //[0.0 0.1 0.125 0.15 0.175 0.2 0.225 0.25 0.275 0.3 0.325 0.35 0.375 0.4 0.6 0.65 0.8 1.0 1.2 1.4 1.6 1.8 2.0 2.2 2.4 2.6 2.8 3.0]
    #define waterCB 0.2	        //[0.0 0.1 0.125 0.15 0.175 0.2 0.225 0.25 0.275 0.3 0.325 0.35 0.375 0.4 0.6 0.8 1.0 1.2 1.25 1.4 1.6 1.8 2.0 2.2 2.4 2.6 2.8 3.0]
    #define waterA 6.0	        //[1.0 1.5 2.0 2.5 3.0 3.5 4.0 4.5 5.0 5.5 6.0 7.0 8.0 9.0 10.0]

    #define emissive_R 1.5		//[0.0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0]
    #define emissive_G 0.42		//[0.0 0.1 0.2 0.3 0.42 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0]
    #define emissive_B 0.045	//[0.0 0.045 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0]
#endif

#ifdef gbuffers_weather
    //#define WeatherAngle		//Toggle alternative weather angle
#endif

#ifdef lightingColors
	#define light_brightness 1.0 //[0.01 0.02 0.03 0.04 0.05 0.06 0.07 0.08 0.09 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0]
    #define r_multiplier 0.0	//[0.0 0.25 0.5 0.75 1.0 1.25 1.5 1.75 2.0 2.25 2.5 2.75 3.0 3.25 3.5 3.75 4.0 4.25 4.5 4.75 5.0 5.25 5.5 5.75 6.0 6.25 6.5 6.75 7.0 7.25 7.5 7.75 8.0 8.25 8.5 8.75 9.0 9.25 9.5 9.75 10.0]
	#define g_multiplier 0.0	//[0.0 0.25 0.5 0.75 1.0 1.25 1.5 1.75 2.0 2.25 2.5 2.75 3.0 3.25 3.5 3.75 4.0 4.25 4.5 4.75 5.0 5.25 5.5 5.75 6.0 6.25 6.5 6.75 7.0 7.25 7.5 7.75 8.0 8.25 8.5 8.75 9.0 9.25 9.5 9.75 10.0]
	#define b_multiplier 0.0	//[0.0 0.25 0.5 0.75 1.0 1.25 1.5 1.75 2.0 2.25 2.5 2.75 3.0 3.25 3.5 3.75 4.0 4.25 4.5 4.75 5.0 5.25 5.5 5.75 6.0 6.25 6.5 6.75 7.0 7.25 7.5 7.75 8.0 8.25 8.5 8.75 9.0 9.25 9.5 9.75 10.0]
#endif

varying vec2 texcoord;

uniform sampler2D gaux4;	//final image

#ifdef Bloom
uniform sampler2D colortex6; //overwritten by bloom
#endif

#if Showbuffer > 0
uniform sampler2D colortex0;
uniform sampler2D colortex1;
uniform sampler2D colortex2;
uniform sampler2D colortex3;
uniform sampler2D gaux1;
uniform sampler2D gaux2;
uniform sampler2D gaux3;
#endif
uniform int isEyeInWater;

uniform float aspectRatio;
uniform float viewWidth;
uniform float viewHeight;
uniform float rainStrength;
uniform float frameTimeCounter;

#ifdef Rain_Drops
varying vec2 rainPos1;
varying vec2 rainPos2;
varying vec2 rainPos3;
varying vec2 rainPos4;
varying vec4 weights;
#endif

#if defined Depth_of_Field || defined Motionblur || defined Cloudsblur
uniform sampler2D depthtex0;
uniform sampler2D depthtex1;
uniform sampler2D depthtex2;
#endif

#ifdef Motionblur
uniform vec3 cameraPosition; 
uniform vec3 previousCameraPosition;
uniform mat4 gbufferProjection;
uniform mat4 gbufferProjectionInverse;
uniform mat4 gbufferPreviousProjection;
uniform mat4 gbufferPreviousModelView;
#endif
uniform mat4 gbufferModelViewInverse;

#if defined Depth_of_Field || defined Cloudsblur
uniform float near;
uniform float far;
float ld(float depth) {
    return (2.0 * near) / (far + near - depth * (far - near));
}
#endif

#ifdef Depth_of_Field
//Dof constant values
const float focal = 0.024;
float aperture = 0.008;	
const float sizemult = DoF_Strength;
uniform float centerDepthSmooth; 
const float centerDepthHalflife = 2.0f; 
	//hexagon pattern
	const vec2 hex_offsets[60] = vec2[60] (	vec2(  0.2165,  0.1250 ),
											vec2(  0.0000,  0.2500 ),
											vec2( -0.2165,  0.1250 ),
											vec2( -0.2165, -0.1250 ),
											vec2( -0.0000, -0.2500 ),
											vec2(  0.2165, -0.1250 ),
											vec2(  0.4330,  0.2500 ),
											vec2(  0.0000,  0.5000 ),
											vec2( -0.4330,  0.2500 ),
											vec2( -0.4330, -0.2500 ),
											vec2( -0.0000, -0.5000 ),
											vec2(  0.4330, -0.2500 ),
											vec2(  0.6495,  0.3750 ),
											vec2(  0.0000,  0.7500 ),
											vec2( -0.6495,  0.3750 ),
											vec2( -0.6495, -0.3750 ),
											vec2( -0.0000, -0.7500 ),
											vec2(  0.6495, -0.3750 ),
											vec2(  0.8660,  0.5000 ),
											vec2(  0.0000,  1.0000 ),
											vec2( -0.8660,  0.5000 ),
											vec2( -0.8660, -0.5000 ),
											vec2( -0.0000, -1.0000 ),
											vec2(  0.8660, -0.5000 ),
											vec2(  0.2163,  0.3754 ),
											vec2( -0.2170,  0.3750 ),
											vec2( -0.4333, -0.0004 ),
											vec2( -0.2163, -0.3754 ),
											vec2(  0.2170, -0.3750 ),
											vec2(  0.4333,  0.0004 ),
											vec2(  0.4328,  0.5004 ),
											vec2( -0.2170,  0.6250 ),
											vec2( -0.6498,  0.1246 ),
											vec2( -0.4328, -0.5004 ),
											vec2(  0.2170, -0.6250 ),
											vec2(  0.6498, -0.1246 ),
											vec2(  0.6493,  0.6254 ),
											vec2( -0.2170,  0.8750 ),
											vec2( -0.8663,  0.2496 ),
											vec2( -0.6493, -0.6254 ),
											vec2(  0.2170, -0.8750 ),
											vec2(  0.8663, -0.2496 ),
											vec2(  0.2160,  0.6259 ),
											vec2( -0.4340,  0.5000 ),
											vec2( -0.6500, -0.1259 ),
											vec2( -0.2160, -0.6259 ),
											vec2(  0.4340, -0.5000 ),
											vec2(  0.6500,  0.1259 ),
											vec2(  0.4325,  0.7509 ),
											vec2( -0.4340,  0.7500 ),
											vec2( -0.8665, -0.0009 ),
											vec2( -0.4325, -0.7509 ),
											vec2(  0.4340, -0.7500 ),
											vec2(  0.8665,  0.0009 ),
											vec2(  0.2158,  0.8763 ),
											vec2( -0.6510,  0.6250 ),
											vec2( -0.8668, -0.2513 ),
											vec2( -0.2158, -0.8763 ),
											vec2(  0.6510, -0.6250 ),
											vec2(  0.8668,  0.2513 ));								
#endif

#ifdef Cloudsblur
float comp = 1.0-near/far/far;
vec3 cblur(vec2 tc){
	float pw = 1.0 / viewWidth;
	float getdist = 1.0-(exp(-pow(ld(texture2D(depthtex1, tc).r)/256.0*far,4.0)*4.0));
	float pcoc = min(getdist*pw*20.0,pw*20.0);
	vec2 fast_blur[4] = vec2[4](vec2(0.0, -0.1),
								vec2(-0.1, 0.0),
								vec2(0.1, 0.0),
								vec2(0.0, 0.1));
	vec3 blurC = vec3(0.0);
	for (int i = 0; i < 4; i++) {
		blurC += texture2D(gaux4, tc + fast_blur[i]*pcoc*vec2(1.0,aspectRatio)).rgb;
	}
		blurC = blurC/4.0*50.0;

	return blurC;
}
#endif

#ifdef Rain_Drops
float distratio(vec2 pos, vec2 pos2) {
	return distance(pos*vec2(aspectRatio,1.0),pos2*vec2(aspectRatio,1.0));
}
float gen_circular_lens(vec2 center, float size) {
	float dist=distratio(center,texcoord.xy)/size;
	return exp(-dist*dist);
}
#endif

vec3 Uncharted2Tonemap(vec3 x) {
	x*= Brightness;
	float A = 0.28;
	float B = 0.29;		
	float C = 0.10;
	float D = 0.2;
	float E = 0.025;
	float F = 0.35;
	return ((x*(A*x+C*B)+D*E)/(x*(A*x+B)+D*F))-E/F;
}

#if Showbuffer == 1 || Showbuffer == 2 || Showbuffer == 3
vec3 decode (vec2 enc){
    vec2 fenc = enc*4-2;
    float f = dot(fenc,fenc);
    float g = sqrt(1-f/4.0);
    vec3 n;
    n.xy = fenc*g;
    n.z = 1-f/2;
    return n;
}
vec3 YCoCg2RGB(vec3 c){
	c.y-=0.5;
	c.z-=0.5;
	return vec3(c.r+c.g-c.b, c.r + c.b, c.r - c.g - c.b);
}
#endif

void main() {

#if defined Depth_of_Field || defined Motionblur || defined Cloudsblur
//Setup depths, do it here because amd drivers suck and texture reads outside of void main or functions are broken, thanks amd
float depth1 = texture2D(depthtex1, texcoord).x;
bool hand = !(depth1 < texture2D(depthtex2, texcoord).x); //is not hand cuz ! 
#endif

//Rainlens
float rainlens = 0.0;
#ifdef Rain_Drops	
	if (rainStrength > 0.02) {
		rainlens += gen_circular_lens(rainPos1,0.1)*weights.x;
		rainlens += gen_circular_lens(rainPos2,0.07)*weights.y;
		rainlens += gen_circular_lens(rainPos3,0.086)*weights.z;
		rainlens += gen_circular_lens(rainPos4,0.092)*weights.w;
	}/*----------------------------------------------------------*/
#endif

	vec2 fake_refract = vec2(0.0);
#ifdef Refraction
		fake_refract = vec2(sin(frameTimeCounter + texcoord.x*100.0 + texcoord.y*50.0),cos(frameTimeCounter + texcoord.y*100.0 + texcoord.x*50.0));
#endif
	vec2 newTC = clamp(texcoord + fake_refract * 0.01 * (rainlens+isEyeInWater*0.2),1.0/vec2(viewWidth,viewHeight),1.0-1.0/vec2(viewWidth,viewHeight));
	vec3 color = texture2D(gaux4, newTC.xy).rgb*50.0;

#ifdef Cloudsblur
#if defined MC_GL_VENDOR_ATI || defined MC_OS_LINUX
	color.rgb = cblur(newTC.xy);	//checking only for sky causes weird outlines on amd cards and linux drivers
#else
	if(depth1 > comp)color.rgb = cblur(newTC.xy);
#endif	
#endif

#ifdef Depth_of_Field
if(hand){
	float pw = 1.0/ viewWidth;
	float z = ld(texture2D(depthtex0, newTC.st).r)*far;
	#ifdef smoothDof
	float focus = ld(centerDepthSmooth)*far;
	#else
	float focus = ld(texture2D(depthtex0, vec2(0.5)).r)*far;
	#endif
	float pcoc = min(abs(aperture * (focal * (z - focus)) / (z * (focus - focal)))*sizemult,pw*15.0);
#ifdef Distance_Blur
	float getdist = 1-(exp(-pow(ld(texture2D(depthtex1, newTC.st).r)/Dof_Distance_View*far,4.0-(2.7*rainStrength))*4.0));	
	pcoc = min(getdist*pw*20.0,pw*20.0);
#endif
	vec3 bcolor = vec3(0.0);
		for ( int i = 0; i < 60; i++) {
			bcolor += texture2D(gaux4, newTC.xy + hex_offsets[i]*pcoc*vec2(1.0,aspectRatio)).rgb;
			}
		color.rgb = bcolor/61.0*50.0;
}
#endif
	
#ifdef Motionblur
if(hand){
	vec4 currentPosition = vec4(texcoord, depth1, 1.0)*2.0-1.0;
	
	vec4 fragposition = gbufferProjectionInverse * currentPosition;
		 fragposition = gbufferModelViewInverse * fragposition;
		 fragposition /= fragposition.w;
		 fragposition.xyz += cameraPosition;
	
	vec4 previousPosition = fragposition;
		 previousPosition.xyz -= previousCameraPosition;
		 previousPosition = gbufferPreviousModelView * previousPosition;
		 previousPosition = gbufferPreviousProjection * previousPosition;
		 previousPosition /= previousPosition.w;

	vec2 velocity = (currentPosition - previousPosition).st * MB_strength;
	vec2 coord = texcoord.st + velocity;

	int mb = 1;
	for (int i = 0; i < 15; ++i, coord += velocity) {
		if (coord.s > 1.0 || coord.t > 1.0 || coord.s < 0.0 || coord.t < 0.0) break;
		color += texture2D(gaux4, coord).xyz*50.0;
		++mb;
	}
	color /= mb;
}
#endif

#ifdef Bloom
	color.rgb += texture2D(colortex6, texcoord.xy*0.25).rgb; //upscale bloom buffer.
#endif
	color.rgb += rainlens*0.01; //draw rainlens
	
	vec3 curr = Uncharted2Tonemap(color*4.7);
	color = pow(curr/Uncharted2Tonemap(vec3(15.2)),vec3(1.0/Contrast));

#if Showbuffer == 1
	color = vec3(texture2D(colortex0,texcoord).rg,0.0);

	vec2 a0 = texture2D(colortex0,texcoord + vec2(1.0/viewWidth,0.0)).rg;
	vec2 a1 = texture2D(colortex0,texcoord - vec2(1.0/viewWidth,0.0)).rg;
	vec2 a2 = texture2D(colortex0,texcoord + vec2(0.0,1.0/viewHeight)).rg;
	vec2 a3 = texture2D(colortex0,texcoord - vec2(0.0,1.0/viewHeight)).rg;
	vec4 lumas = vec4(a0.x,a1.x,a2.x,a3.x);
	vec4 chromas = vec4(a0.y,a1.y,a2.y,a3.y);

	vec4 w = 1.0-step(0.1176, abs(lumas - color.x));
	float W = dot(w,vec4(1.0));
	w.x = (W==0.0)? 1.0:w.x;  W = (W==0.0)? 1.0:W;

	bool pattern = (mod(gl_FragCoord.x,2.0)==mod(gl_FragCoord.y,2.0));
	color.b= dot(w,chromas)/W;
	color.rgb = (pattern)?color.rbg:color.rgb;
	color.rgb = YCoCg2RGB(color.rgb);
	color = pow(color,vec3(2.2));
#endif
#if Showbuffer == 2
	color = decode(texture2D(colortex1, texcoord).xy);
#endif
#if Showbuffer == 25
	color = vec3(0.0, texture2D(colortex1, texcoord.xy).zw);	//lightmap
#endif
#if Showbuffer == 3
	color = decode(texture2D(colortex2, texcoord).xy);
#endif
#if Showbuffer == 35
	color = vec3(0.0, texture2D(colortex2, texcoord.xy).zw);	//lightmap	
#endif	
#if Showbuffer == 4	
	color = texture2D(colortex3, texcoord.xy).rgb * 200.0;
#endif	
#if Showbuffer == 5	
	color = texture2D(gaux1, texcoord.xy).rgb * 25.0;
#endif	
#if Showbuffer == 6	
	color = texture2D(gaux2, texcoord.xy).rgb * 25.0;
#endif	
#if Showbuffer == 7	
	color = texture2D(gaux3, texcoord.xy*0.25).rgb * 25.0;
#endif	
#if Showbuffer == 8
	color = texture2D(gaux4, texcoord.xy).rgb * 50.0;
#endif

	gl_FragColor = vec4(color,1.0);
}

